<?php
/**
 * Created by PhpStorm.
 * User: Shuvo
 * Date: 12/7/2018
 * Time: 12:42 AM
 */

namespace App;
if ( ! isset( $_SESSION ) ) {
	session_start();
}

class Message {
	public static function message( $msg = null ) {

		if ( is_null( $msg ) ) {

			return self::getMessage();
		} else {
			self::setMessage( $msg );
		}

	}


	public static function setMessage( $msg ) {

		$_SESSION['message'] = $msg;
	}


	public static function getMessage() {
		if ( ! isset( $_SESSION['message'] ) ) {
			$_SESSION['message'] = "";
		}


		$tempMSG             = $_SESSION['message'];
		$_SESSION['message'] = "";

		return $tempMSG;

	}

}