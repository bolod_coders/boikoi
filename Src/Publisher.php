<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/23/2018
 * Time: 10:39 PM
 */

namespace App;

class Publisher extends Database {
	private $id;
	private $name;
	private $nameEn;
	private $logo;






    public function setData( $postArray ) {

        if ( array_key_exists( 'publisherId', $postArray ) ) {
            $this->id = $postArray['publisherId'];
        }

        if ( array_key_exists( 'publisherName', $postArray ) ) {
            $this->name = $postArray['categoryName'];
        }
        if ( array_key_exists( 'publisherNameEn', $postArray ) ) {
            $this->nameEn = $postArray['publisherNameEn'];
        }

        if ( array_key_exists( 'publisherLogo', $postArray ) ) {
            $this->logo = $postArray['categoryLogo'];
        }




    }







    /**
	 * Publisher constructor.
	 *
	 * @param $id
	 * @param $name
	 * @param $logo
	 */
	public function __construct( $id, $name,$nameEn, $logo ) {
		parent::__construct();
		$this->id   = $id;
		$this->name = $name;
		$this->nameEn = $nameEn;
		$this->logo = $logo;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName( $name ) {
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getLogo() {
		return $this->logo;
	}

	/**
	 * @param mixed $logo
	 */
	public function setLogo( $logo ) {
		$this->logo = $logo;
	}

	public function showPublisherList() {

		$sqlStatemnet = "select * from publishers";
		$sth          = $this->dbh->query( $sqlStatemnet );

		return $sth->fetchAll( \PDO::FETCH_OBJ );
	}


	public function paginateActiveList( $offset, $itemPerPage ) {

		$sql = "Select * from publishers LIMIT $offset,$itemPerPage";

		$sth = $this->dbh->query( $sql );

		$someRecords = $sth->fetchAll( \PDO::FETCH_OBJ );

		return $someRecords;


	}


    public function delete_publisher( $publisher_id ) {

        $sqlstatement = "DELETE FROM `publishers` WHERE `publishers`.`publisher_id` =$publisher_id";

        $sth = $this->dbh->prepare( $sqlstatement );

        $result = $sth->execute();

        if ( $result ) {

            Message::message( "publisher has been deleted successfully<br>" );
        } else {
            Message::message( "publisher has not been deleted due to an error <br>" );
        }

    }



    public function admin_add_new_publisher() {

        $dataArray = [
            $this->name,
            $this->nameEn,
            $this->logo


        ];

        $sqlStatement = "INSERT INTO `publishers`
					( `publisher_name`,`publisher_name_en`,`publisher_logo`) 
						VALUES 
					(?,?,?);";

        $sth = $this->dbh->prepare( $sqlStatement );

        $result = $sth->execute( $dataArray );

        if ( $result ) {

            Message::message( "publisher has been inserted successfully<br>" );
        } else {
            Message::message( "publisher has not been inserted due to an error <br>" );
        }

    }

}