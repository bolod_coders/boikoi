<?php
/**
 * Created by PhpStorm.
 * User: Shuvo
 * Date: 12/6/2018
 * Time: 11:23 PM
 */

namespace App;


class Person extends Database {
	private $name;
	private $email;
	private $password;


	/**
	 * Person constructor.
	 *
	 * @param $name
	 * @param $email
	 * @param $password
	 */
	public function __construct( $name, $email, $password ) {
		parent::__construct();
		$this->name     = $name;
		$this->email    = $email;
		$this->password = $password;
	}


	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName( $name ) {
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail( $email ) {
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param mixed $password
	 */
	public function setPassword( $password ) {
		$this->password = $password;
	}


	public function insertRegisterUser() {

		$dataArray = [ $this->name, $this->email, $this->password ];
		$stmnt     = "INSERT INTO `users` (`user_full_name`,`user_email`,`user_password`) VALUES (?,?,?)";

		$sth    = $this->dbh->prepare( $stmnt );
		$result = $sth->execute( $dataArray );


		if ( $result ) {

			Message::message( 'Successfully Registered.\n Check your email(' . $this->email . ') for verification!' );

		} else {
			Message::message( 'Registration Failed! Go to Hell!' );
		}

	}


	public function emailMatching( $email ) {
		$query = "SELECT user_email FROM users where user_email='$email'";

		$queryStrng = $this->dbh->query( $query );
		$fatchstrng = $queryStrng->fetch( \PDO::FETCH_OBJ );

		return $fatchstrng;

	}

	public function verify( $email, $token ) {


		$password = explode( $email, $token );


		$query = "SELECT user_email,user_password FROM users where user_email='$email' AND user_password='$password[0]'";

		$queryStrng = $this->dbh->query( $query );
		$fatchstrng = $queryStrng->fetch( \PDO::FETCH_OBJ );

		return $fatchstrng;


	}

	public function verifyRegistration( $token, $email, $password ) {

		$update = "UPDATE `users` SET `user_is_verified`= '$token' WHERE user_email='$email' AND user_password='$password'";

		$sth    = $this->dbh->prepare( $update );
		$result = $sth->execute();


		if ( $result ) {

			Message::message( 'Successfully Verified.You can log in now' );

		} else {
			Message::message( 'Verification Failed! Go to Hell!mfckr' );
		}


	}

	public function loginVerify( $email ) {

		$query = "SELECT user_email,user_password FROM users where user_email='$email'AND user_is_verified != 'no'";

		$queryStrng = $this->dbh->query( $query );
		$fatchstrng = $queryStrng->fetch( \PDO::FETCH_OBJ );

		return $fatchstrng;


	}

	public function recoverPassEmail( $email ) {
		$query = "SELECT user_full_name,user_email,user_password FROM users where user_email='$email'AND user_is_verified != 'no'";

		$queryStrng = $this->dbh->query( $query );
		$fatchstrng = $queryStrng->fetch( \PDO::FETCH_OBJ );


		if ( $fatchstrng ) {

			Message::message( 'Check Your Email And click the link for changing Password' );

		} else {
			Message::message( 'Email not Registered' );
		}

		return $fatchstrng;
	}


	public function setNewPassword( $password, $email ) {
		$update = "UPDATE `users` SET `user_password`= '$password' WHERE user_email='$email'";

		$sth    = $this->dbh->prepare( $update );
		$result = $sth->execute();


		if ( $result ) {

			Message::message( 'Successfully Verified.You can log in now' );

		} else {
			Message::message( 'Password changed Failed!' );
		}
	}


	public function checkAdmin( $email ) {

		$sqlstmnt = "select * from users where user_role = 'admin' AND user_email = '$email'";


		$queryStrng = $this->dbh->query( $sqlstmnt );
		$fatchstrng = $queryStrng->fetch( \PDO::FETCH_OBJ );


		return $fatchstrng;

	}



}