<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/23/2018
 * Time: 10:38 PM
 */

namespace App;

class Category extends Database {

	private $id;
	private $name;
	private $nameEn;



    public function setData( $postArray ) {

        if ( array_key_exists( 'categoryId', $postArray ) ) {
            $this->id = $postArray['categoryId'];
        }

        if ( array_key_exists( 'categoryName', $postArray ) ) {
            $this->name = $postArray['categoryName'];
        }
         if ( array_key_exists( 'categoryNameEn', $postArray ) ) {
                    $this->nameEn = $postArray['categoryNameEn'];
                }



    }



    /**
	 * Category constructor.
	 *
	 * @param $id
	 * @param $name
	 */
	public function __construct( $id, $name,$nameEn ) {
		parent::__construct();
		$this->id   = $id;
		$this->name = $name;
		$this->nameEn = $nameEn;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName( $name ) {
		$this->name = $name;
	}

	public function showCategoryList() {

		$sqlStatemnet = "select * from categories";
		$sth          = $this->dbh->query( $sqlStatemnet );

		return $sth->fetchAll( \PDO::FETCH_OBJ );
	}

	public function paginateActiveList( $offset, $itemPerPage ) {

		$sql = "Select * from categories LIMIT $offset,$itemPerPage";

		$sth = $this->dbh->query( $sql );

		$someRecords = $sth->fetchAll( \PDO::FETCH_OBJ );

		return $someRecords;


	}


    public function delete_category( $category_id ) {

        $sqlstatement = "DELETE FROM `categories` WHERE `categories`.`category_id` =$category_id";

        $sth = $this->dbh->prepare( $sqlstatement );

        $result = $sth->execute();

        if ( $result ) {

            Message::message( "category has been deleted successfully<br>" );
        } else {
            Message::message( "category has not been deleted due to an error <br>" );
        }

    }

    public function admin_add_new_category() {

        $dataArray = [
            $this->name,
            $this->nameEn


        ];

        $sqlStatement = "INSERT INTO `categories`
					( `category_name`,`category_name_en`) 
						VALUES 
					(?,?);";

        $sth = $this->dbh->prepare( $sqlStatement );

        $result = $sth->execute( $dataArray );

        if ( $result ) {

            Message::message( "category has been inserted successfully<br>" );
        } else {
            Message::message( "category has not been inserted due to an error <br>" );
        }

    }
}