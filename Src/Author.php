<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/23/2018
 * Time: 8:45 PM
 */

namespace App;

class Author extends Database {
	private $id;
	private $name;
	private $nameEn;
	private $details;
	private $image;


    public function setData( $postArray ) {

        if ( array_key_exists( 'authorId', $postArray ) ) {
            $this->id = $postArray['authorId'];
        }

        if ( array_key_exists( 'authorName', $postArray ) ) {
            $this->name = $postArray['authorName'];
        }
        if ( array_key_exists( 'authorNameEn', $postArray ) && $postArray['authorNameEn'] != "" ) {
            $this->nameEn = $postArray['authorNameEn'];
        }
        if ( array_key_exists( 'authorCover', $postArray ) && $postArray['authorCover'] != "" ) {
            $this->image = $postArray['authorCover'];
        }
        if ( array_key_exists( 'authorDetails', $postArray ) && $postArray['authorDetails'] != "" ) {
            $this->details = $postArray['authorDetails'];
        }


    }



	/**
	 * Author constructor.
	 *
	 * @param $id
	 * @param $name
	 * @param $details
	 * @param $image
	 */


	public function __construct( $id, $name,$nameEn, $details, $image ) {
		parent::__construct();
		$this->id      = $id;
		$this->name    = $name;
		$this->nameEn    = $nameEn;
		$this->details = $details;
		$this->image   = $image;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName( $name ) {
		$this->name = $name;
	}
	/**
	 * @return mixed
	 */
	public function getNameEn() {
		return $this->nameEn;
	}

	/**
	 * @param mixed $name
	 */
	public function setNameEn( $name ) {
		$this->nameEn = $name;
	}

	/**
	 * @return mixed
	 */
	public function getDetails() {
		return $this->details;
	}

	/**
	 * @param mixed $details
	 */
	public function setDetails( $details ) {
		$this->details = $details;
	}

	/**
	 * @return mixed
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * @param mixed $image
	 */
	public function setImage( $image ) {
		$this->image = $image;
	}

	public function showAuthorList() {

		$sqlStatemnet = "select * from authors";
		$sth          = $this->dbh->query( $sqlStatemnet );

		return $sth->fetchAll( \PDO::FETCH_OBJ );
	}


	public function paginateActiveList( $offset, $itemPerPage ) {

		$sql = "Select * from authors LIMIT $offset,$itemPerPage";

		$sth = $this->dbh->query( $sql );

		$someRecords = $sth->fetchAll( \PDO::FETCH_OBJ );

		return $someRecords;


	}

    public function admin_add_new_author() {

        $dataArray = [
            $this->name,
            $this->nameEn,
            $this->image,
            $this->details,

        ];

        $sqlStatement = "INSERT INTO `authors`
					( `author_name`, `author_name_en`, 
					  `author_image`, 
					  `author_details`) 
						VALUES 
					( ?, ?, 
					  ?, 
					  ?);";

        $sth = $this->dbh->prepare( $sqlStatement );

        $result = $sth->execute( $dataArray );

        if ( $result ) {

            Message::message( "Author has been inserted successfully<br>" );
        } else {
            Message::message( "Author has not been inserted due to an error <br>" );
        }

    }

    public function view_author() {

        $sqlstatement = "SELECT * FROM `authors`
                        
                           WHERE `authors`.`author_id` =$this->id";


        $sth = $this->dbh->query( $sqlstatement );

        return $sth->fetch( \PDO::FETCH_OBJ );


    }


    public function update_author() {
        $dataArray    = [
            $this->name,
            $this->nameEn,
            $this->image,
            $this->details,


        ];
        $sqlStatement = "UPDATE `authors` SET 
					 `author_name`=?, `author_name_en`=?, 
					  `author_image`=?, 
					  `author_details`=?
						 WHERE `authors`.`author_id` = $this->id";

        $sth = $this->dbh->prepare( $sqlStatement );

        $result = $sth->execute( $dataArray );

        if ( $result ) {

            Message::message( "Author has been updated successfully<br>" );
        } else {
            Message::message( "Author has not been updated due to an error <br>" );
        }
    }



    public function delete_author( $author_id ) {

        $sqlstatement = "DELETE FROM `authors` WHERE `authors`.`author_id` =$author_id";

        $sth = $this->dbh->prepare( $sqlstatement );

        $result = $sth->execute();

        if ( $result ) {

            Message::message( "author has been deleted successfully<br>" );
        } else {
            Message::message( "author has not been deleted due to an error <br>" );
        }

    }


}