<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/24/2018
 * Time: 9:47 PM
 */


namespace App;

class Book extends Database {
	private $id;
	private $name;
	private $nameEn;
	private $cover;
	private $category;
	private $author;
	private $publisher;
	private $price;
	private $page;
	private $in_stock;
	private $isbn;
	private $is_off;
	private $language;
	private $edition;
	private $summary;
	private $is_publish;
	private $country;
	private $type;


	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}


	public function setData( $postArray ) {

		if ( array_key_exists( 'bookId', $postArray ) ) {
			$this->id = $postArray['bookId'];
		}

		if ( array_key_exists( 'bookName', $postArray ) ) {
			$this->name = $postArray['bookName'];
		}
		if ( array_key_exists( 'bookNameEn', $postArray ) && $postArray['bookNameEn'] != "" ) {
			$this->nameEn = $postArray['bookNameEn'];
		}
		if ( array_key_exists( 'bookCover', $postArray ) && $postArray['bookCover'] != "" ) {
			$this->cover = $postArray['bookCover'];
		}
		if ( array_key_exists( 'bookAuthor', $postArray ) && $postArray['bookAuthor'] != "" ) {
			$this->author = $postArray['bookAuthor'];
		}
		if ( array_key_exists( 'bookCategory', $postArray ) && $postArray['bookCategory'] != "" ) {
			$this->category = $postArray['bookCategory'];
		}
		if ( array_key_exists( 'bookPublisher', $postArray ) && $postArray['bookPublisher'] != "" ) {
			$this->publisher = $postArray['bookPublisher'];
		}
		if ( array_key_exists( 'bookPrice', $postArray ) && $postArray['bookPublisher'] != "" ) {
			$this->price = $postArray['bookPrice'];
		}
		if ( array_key_exists( 'bookISBN', $postArray ) && $postArray['bookISBN'] != "" ) {
			$this->isbn = $postArray['bookISBN'];
		}
		if ( array_key_exists( 'bookEdition', $postArray ) && $postArray['bookEdition'] != "" ) {
			$this->edition = $postArray['bookEdition'];
		}
		if ( array_key_exists( 'bookPage', $postArray ) && $postArray['bookPage'] != "" ) {
			$this->page = $postArray['bookPage'];
		}
		if ( array_key_exists( 'bookLanguage', $postArray ) && $postArray['bookLanguage'] != "" ) {
			$this->language = $postArray['bookLanguage'];
		}
		if ( array_key_exists( 'bookOff', $postArray ) && $postArray['bookOff'] != "" ) {
			$this->is_off = $postArray['bookOff'];
		}
		if ( array_key_exists( 'bookInStock', $postArray ) && $postArray['bookInStock'] != "" ) {
			$this->in_stock = $postArray['bookInStock'];
		}
		if ( array_key_exists( 'bookSummary', $postArray ) && $postArray['bookSummary'] != "" ) {
			$this->summary = $postArray['bookSummary'];
		}
		if ( array_key_exists( 'bookPublish', $postArray ) && $postArray['bookPublish'] != "" ) {
			$this->is_publish = $postArray['bookPublish'];
		}
		if ( array_key_exists( 'bookCountry', $postArray ) && $postArray['bookCountry'] != "" ) {
			$this->country = $postArray['bookCountry'];
		}
		if ( array_key_exists( 'bookType', $postArray ) && $postArray['bookType'] != "" ) {
			$this->type = $postArray['bookType'];
		}


	}

	/* Client View */
	public function index() {
		$sqlStatemnet = "select * from books 
						INNER JOIN authors ON books.book_author = authors.author_id
					  	WHERE books.book_is_published = '1'";
		$sth          = $this->dbh->query( $sqlStatemnet );

		return $sth->fetchAll( \PDO::FETCH_OBJ );

	}


	public function homepage_latestBooks() {
		$sqlStatemnet = "select * from books INNER JOIN authors ON books.book_author = authors.author_id
					  	WHERE books.book_is_published = '1'
					  	LIMIT 12";
		$sth          = $this->dbh->query( $sqlStatemnet );

		return $sth->fetchAll( \PDO::FETCH_OBJ );

	}

	/*
		public function homepage_popularBooks() { //
			$sqlStatemnet = "select * from books INNER JOIN authors ON books.book_author = authors.author_id ORDER BY books.book_sold DESC LIMIT 12";
			$this->dbh->query( "SET NAMES UTF8" );
			$sth = $this->dbh->query( $sqlStatemnet );

			return $sth->fetchAll( \PDO::FETCH_OBJ );

		}*/

	/* Admin Dashboard */

	/* Read Book List*/
	public function admin_bookList() {
		$sqlStatemnet = "SELECT * 
						FROM books
						INNER JOIN authors ON book_author = authors.author_id 
						INNER JOIN categories ON book_category = categories.category_id
						INNER JOIN publishers ON book_publisher = publishers.publisher_id
						ORDER BY books.book_id DESC;";
		$sth          = $this->dbh->query( $sqlStatemnet );

		return $sth->fetchAll( \PDO::FETCH_OBJ );

	}

	/* Add New Book */

	public function admin_add_new_Books() {

		$dataArray = [
			$this->name,
			$this->nameEn,
			$this->cover,
			$this->author,
			$this->category,
			$this->publisher,
			$this->price,
			$this->isbn,
			$this->edition,
			$this->page,
			$this->in_stock,
			$this->language,
			$this->is_off,
			$this->summary,
			$this->is_publish,
			$this->country,
			$this->type
		];

		$sqlStatement = "INSERT INTO `books`
					( `book_name`, `book_name_en`, 
					  `book_cover`, 
					  `book_author`, `book_category`, `book_publisher`, 
					  `book_price`, `book_isbn`,`book_edition`,`book_page`,
					  `book_in_stock`,`book_language`, `book_is_off`,
					  `book_summary`, `book_is_published`, `book_country`, `book_type`) 
						VALUES 
					( ?, ?, 
					  ?, 
					  ?, ?, ?,
					  ?, ?, ?, ?, 
					  ?, ?, ?,
					  ?, ?, ?, ?);";

		$sth = $this->dbh->prepare( $sqlStatement );

		$result = $sth->execute( $dataArray );

		if ( $result ) {

			Message::message( "Book has been inserted successfully<br>" );
		} else {
			Message::message( "Book has not been inserted due to an error <br>" );
		}

	}


	public function delete_book( $book_id ) {

		$sqlstatement = "DELETE FROM `books` WHERE `books`.`book_id` =$book_id";

		$sth = $this->dbh->prepare( $sqlstatement );

		$result = $sth->execute();

		if ( $result ) {

			Message::message( "Book has been deleted successfully<br>" );
		} else {
			Message::message( "Book has not been deleted due to an error <br>" );
		}

	}

	/*public function edit_book()
	{

		$dataArray = [
			$this->name,
			$this->nameEn,
			$this->cover,
			$this->author,
			$this->category,
			$this->publisher,
			$this->price,
			$this->isbn,
			$this->edition,
			$this->page,
			$this->in_stock,
			$this->language,
			$this->is_off
		];

		$sqlStatement = "UPDATE `books`  SET `book_name`=?, `book_name_en`=?, `book_cover`=?, `book_author`=?, `book_category`=?, `book_publisher`=?, `book_price`=?, `book_isbn`=?,`book_edition`=?,`book_page`=?,`book_in_stock`=?,`book_language`=?,`book_is_off`=? where id=?;";

		$sth = $this->dbh->prepare( $sqlStatement );

		$result = $sth->execute($dataArray);

		if ( $result ) {

			Message::message( "Book has been updated successfully<br>" );
		} else {
			Message::message( "Book has not been deleted due to an error <br>" );
		}

	}*/


	/*
	 * Single Book Read
	 * */
	public function view_book() {

		$sqlstatement = "SELECT * FROM `books`
                          INNER JOIN authors ON books.book_author = authors.author_id 
                          INNER JOIN categories ON books.book_category = categories.category_id 
                          INNER JOIN publishers ON books.book_publisher = publishers.publisher_id 
                           WHERE `books`.`book_id` =$this->id";


		$sth = $this->dbh->query( $sqlstatement );

		return $sth->fetch( \PDO::FETCH_OBJ );


	}

	public function update_book() {
		$dataArray    = [
			$this->name,
			$this->nameEn,
			$this->cover,
			$this->author,
			$this->category,
			$this->publisher,
			$this->price,
			$this->isbn,
			$this->edition,
			$this->page,
			$this->in_stock,
			$this->language,
			$this->is_off,

		];
		$sqlStatement = "UPDATE `books` SET 
					 `book_name`=?, `book_name_en`=?, 
					  `book_cover`=?, 
					  `book_author`=?, `book_category`=?, `book_publisher`=?, 
					  `book_price`=?, `book_isbn`=?,`book_edition`=?,`book_page`=?,
					  `book_in_stock`=?,`book_language`=?, `book_is_off`=? 
						 WHERE `books`.`book_id` = $this->id";

		$sth = $this->dbh->prepare( $sqlStatement );

		$result = $sth->execute( $dataArray );

		if ( $result ) {

			Message::message( "Book has been updated successfully<br>" );
		} else {
			Message::message( "Book has not been updated due to an error <br>" );
		}
	}

	public function publish_book( $publishBook ) {

		$sqlStatement = "UPDATE `books` SET `book_is_published` = $publishBook WHERE `books`.`book_id` = $this->id";

		$sth = $this->dbh->prepare( $sqlStatement );

		$sth->execute();

	}
    public function publish_book_selected( $publishBook ) {

        $sqlStatement = "UPDATE `books` SET `book_is_published` = $publishBook WHERE `books`.`book_id` = $this->id";

        $sth = $this->dbh->prepare( $sqlStatement );

        $result = $sth->execute();

        return $result;

    }


	public function search_book( $query ) {
		$sqlstatement = "SELECT * FROM books 
    INNER JOIN authors ON books.book_author = authors.author_id 
    INNER JOIN categories ON books.book_category = categories.category_id 
    INNER JOIN publishers ON books.book_publisher = publishers.publisher_id 
	WHERE 
    books.book_name LIKE '%$query%' OR 
   	books.book_name_en LIKE '%$query%' OR 
    authors.author_name LIKE '%$query%' OR 
    authors.author_name_en LIKE '%$query%' OR
	publishers.publisher_name LIKE '%$query%' OR
    publishers.publisher_name_en LIKE '%$query%' ";


		$sth = $this->dbh->query( $sqlstatement );

		return $sth->fetchAll( \PDO::FETCH_OBJ );

	}


	public function new_realeased(){
        $sqlStatement = "SELECT * FROM books INNER JOIN authors ON books.book_author = authors.author_id 
    INNER JOIN categories ON books.book_category = categories.category_id 
    INNER JOIN publishers ON books.book_publisher = publishers.publisher_id  ORDER BY books.book_id DESC";

        $sth          = $this->dbh->query( $sqlStatement );

        return $sth->fetchAll( \PDO::FETCH_OBJ );
    }


    public function price_low_high(){
        $sqlStatement = "SELECT * FROM books INNER JOIN authors ON books.book_author = authors.author_id 
    INNER JOIN categories ON books.book_category = categories.category_id 
    INNER JOIN publishers ON books.book_publisher = publishers.publisher_id  ORDER BY books.book_price ASC";

        $sth          = $this->dbh->query( $sqlStatement );

        return $sth->fetchAll( \PDO::FETCH_OBJ );
    }

    public function price_high_low(){
        $sqlStatement = "SELECT * FROM books INNER JOIN authors ON books.book_author = authors.author_id 
    INNER JOIN categories ON books.book_category = categories.category_id 
    INNER JOIN publishers ON books.book_publisher = publishers.publisher_id  ORDER BY books.book_price DESC";

        $sth          = $this->dbh->query( $sqlStatement );

        return $sth->fetchAll( \PDO::FETCH_OBJ );
    }

    public function discount_low_high(){
        $sqlStatement = "SELECT * FROM books INNER JOIN authors ON books.book_author = authors.author_id 
    INNER JOIN categories ON books.book_category = categories.category_id 
    INNER JOIN publishers ON books.book_publisher = publishers.publisher_id  ORDER BY books.book_is_off ASC";

        $sth          = $this->dbh->query( $sqlStatement );

        return $sth->fetchAll( \PDO::FETCH_OBJ );
    }

    public function discount_high_low(){
        $sqlStatement = "SELECT * FROM books INNER JOIN authors ON books.book_author = authors.author_id 
    INNER JOIN categories ON books.book_category = categories.category_id 
    INNER JOIN publishers ON books.book_publisher = publishers.publisher_id  ORDER BY books.book_is_off DESC";

        $sth          = $this->dbh->query( $sqlStatement );

        return $sth->fetchAll( \PDO::FETCH_OBJ );
    }


    public function category_list($getArray){

        $sqlStatement = "SELECT * FROM books 
            INNER JOIN authors ON books.book_author = authors.author_id 
             INNER JOIN publishers ON books.book_publisher = publishers.publisher_id
          INNER JOIN categories ON books.book_category = categories.category_id where books.book_category = $getArray";

        $sth          = $this->dbh->query( $sqlStatement );

        return $sth->fetchAll( \PDO::FETCH_OBJ );
    }


}
