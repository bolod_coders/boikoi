<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/6/2018
 * Time: 12:21 AM
 */

namespace App;
class Utility {

	public static function dd( $myVar ) {
		echo "<pre>";
		var_dump( $myVar );
		echo "</pre>";
		die;
	}


	public static function d( $myVar ) {
		echo "<pre>";
		var_dump( $myVar );
		echo "</pre>";
	}


	public static function redirect( $url ) {

		header( 'Location:' . $url );
	}


}