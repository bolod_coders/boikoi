<?php
/**
 * Created by PhpStorm.
 * User: Shuvo
 * Date: 12/22/2018
 * Time: 4:21 AM
 */
require_once '../vendor/autoload.php';
include 'header.php';

$books = new \App\Book();

/* Latest Books */
$latestBooks = $books->homepage_latestBooks();
$latestBooks = json_decode( json_encode( $latestBooks ), true );


?>


<!--<div class="ui-widget">-->
<!--    <label for="tags">Tags: </label>-->
<!--    <input id="tags">-->
<!--</div>-->

<!-- Slider -->

<div id="carouselExampleIndicators" class="carousel slide container mt-5 carousel-fade " data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner shadow-sm rounded">
        <div class="carousel-item active">
            <img class="d-block w-100" src="img/banners/banner1.png" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="img/banners/banner2.png"
                 alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="img/banners/banner3.png" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<!-- Navigation -->

<section id="categories" class="mt-3">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a href="authors.php" class="link">
                    <div class="border p-4 shadow-sm x rounded">

                        <h4><span class="text-muted">Shop by</span> Authors</h4>
                        <p class="text-muted">Browse your favourite authors</p>

                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="categories.php" class="link">
                    <div class="border p-4 shadow-sm x rounded">

                        <h4><span class="text-muted">Shop by</span> Categories</h4>
                        <p class="text-muted">Browse your favourite authors</p>

                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="publishers.php" class="link">
                    <div class="border p-4 shadow-sm x rounded">

                        <h4><span class="text-muted">Shop by</span> Publishers</h4>
                        <p class="text-muted">Browse your favourite authors</p>
                        <i class="ion-chevron-right align-self-center"></i>

                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- Latest Released -->

<section class="container mt-3">
    <div class="bg-white border p-4 shadow-sm rounded">

        <h4 class="pb-2">Latest Books <a class="btn btn-outline-success my-2 my-sm-0 float-right"
                                         href="search.php?orderBy=latest">View All</a></h4>


        <div class="row mt-3 text-center">


			<?php

			foreach ( $latestBooks as $latestBook ) {
				$latestBookId    = $latestBook["book_id"];
				$latestBookCover = $latestBook["book_cover"];
				if ( $latestBookCover == "" || $latestBookCover == null ) {
					$latestBookCover = "default.png";
				}
				$latestBookName   = $latestBook["book_name"];
				$latestBookAuthor = $latestBook["author_name"];
				$latestBookPrice  = $latestBook["book_price"];
				$latestBookOff    = $latestBook["book_is_off"];
				$hideOff          = "";

				if ( $latestBookOff > 0 ) {
					$latestBookPrice = round( $latestBookPrice - ( ( $latestBookPrice * $latestBookOff ) / 100 ) );
				} else {
					$hideOff = "hideOff";
				}

				echo "
	            
            <div class='col-md-3'>
                
                 
                 <div class='product-grid9 overlayNew'>
                    <div class='product-image9 text-center'>
                       <div >
                           <a href='bookDetails.php?book_id=$latestBookId' class='' >
                               <img class='pic-1 img-fluid w-100' src='img/books/$latestBookCover'>
                           </a>
                       </div>

                          <div>
                              <a href='#' class='fa fa-shopping-cart product-full-view button p-2'> Add to Cart</a>
                          </div>
                    </div>
                    <div class='product-content text-center'>
                        <h3 class='title bookTitle'> $latestBookName</h3>
                        <div class='author text-muted authorTitle '>$latestBookAuthor</div>
                        <div class='price mt-3'><b>TK : $latestBookPrice</b> <span class='$hideOff text-danger small'>( $latestBookOff off)</span></div>
                        <a class='add-to-cart btn btn-block buttonView' href='bookDetails.php?book_id=$latestBookId'>View Details</a>
                    </div>
                </div>       

                

            </div>
	            

	            
	            ";
			}

			?>


        </div>


    </div>

</section>


<?php
include 'footer.php';
?>


