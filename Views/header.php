<?php
require_once '../vendor/autoload.php';

if ( ! isset( $_SESSION ) ) {
	session_start();
}

$userEmail = "";

//if User click on a Remember me option then the if block is execute(Cookies) ,otherwise the else block is execute(Session)..
if ( isset( $_COOKIE['user_email'] ) ) {
	$jsonDecodedObj = json_decode( $_COOKIE['user_email'] );
	$userEmail      = $jsonDecodedObj->UserEmail;
} elseif ( isset( $_SESSION['email'] ) ) {
	$userEmail = $_SESSION['email'];
}

$person = new \App\Person( null, null, null );
$person = $person->checkAdmin( $userEmail );

?>
<!doctype html>
<html lang="en">
<head>
    <meta
            http-equiv="Content-Type"
            content="text/html; charset=utf-8"
    />
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../Resource/css/bootstrap.min.css">
    <link rel="stylesheet" href="../Resource/css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">


    <link rel="stylesheet" href="../Resource/jquery_ui/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script>
        $(function () {

            var slider = $("#slider-range");

            slider.slider({
                range: true,
                min: 0,
                max: 500,
                values: [75, 300],
                slide: function (event, ui) {
                    $("#amount").val("TK " + ui.values[0] + " - TK " + ui.values[1]);
                }
            });
            $("#amount").val("TK " + slider.slider("values", 0) +
                " - TK " + slider.slider("values", 1));
        });
    </script>

    <style>
        #response{
            position: absolute;
            top: 101%;
            z-index: 1000;
            background: #f3f3f3;
            width: 64%;
        }

        #response ul{
            border: 1px solid #e0e0e0;
            margin-bottom: 0;
            list-style: none;
            padding: 11px 20px 43px 20px ;
            ba

        }
        #response ul> a >  li:first-child{
            margin-top: -10px;
        }
        #response ul> a >  li:nth-child(2){
            padding-top: 5px;
        }

        #response a{
            color: rgba(0, 0, 0, 0.65);
        }

        #response a:hover{
            color: rgba(231, 44, 44, 0.65);
            font-weight: bold;
        }

    </style>

</head>
<body>


<header class="bg-white shadow-sm rounded-bottom border-bottom">
    <nav class="navbar navbar-expand-lg navbar-light ">
        <div class="container">

            <div class="col-2">
                <a class="navbar-brand" href="index.php"><img src="img/BoiAche3.png" class="navbar-brand"
                                                              width="220px"/></a>
            </div>

            <div class="col-8">

                <form class="form-inline col-md-10 mr-auto ml-5 " action="search.php" method="get" id="searchForm">
                    <input class="form-control mr-sm-2  col-md-8 col-lg-8" type="search"
                           placeholder="Search Books, Authors, Publishers" aria-label="Search"
                           name="search" id="searchID" autocomplete="off">

                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i>
                    </button>
                    <div id="response" ></div>
                </form>

            </div>

            <div class="col-2">

                <ul class="navbar-nav float-right">

                    <li class="nav-item">
                        <a class="btn btn-outline-success my-2 my-sm-0" href="#">Cart</a>
                    </li>


					<?php if ( isset( $_SESSION['email'] ) ) {
						echo "
                        <li class='nav-item'>    
                        <div class='dropdown ml-3'>
                            <a class='btn btn-outline-success dropdown-toggle' href='#' role='button'
                               id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                 $userEmail
                            </a>
                            <div class='dropdown-menu' aria-labelledby='dropdownMenuLink'>
                            ";

						if ( $person != false ) {
							/* Admin */
							echo "
                                    <a class='dropdown-item' href='admin'>Admin Dashboard</a>                                                                                        
                        ";

						} else {
							/* Normal User */
							echo "
						            <a class='dropdown-item' href='#'>My Orders</a>
                                    <a class='dropdown-item' href='#'>My Account</a>

						    ";
						}


						echo "                         
                            <div class='dropdown-divider'></div>
                            <a class='dropdown-item' href='logout.php'>Sign out</a>
                            </div>
                            </div>
                            </li>
                            ";

					} else {
						/* User Logged In or Not*/
						echo "
						    
					    <li class='nav-item ml-3'>
                            <a class='btn btn-outline-success my-2 my-sm-0' href='authentication.php'>Sign in</a>
                        </li>
						    
						    ";
					}
					?>
                    </li>

                </ul>

            </div>

        </div>
    </nav>


</header>

<div id="message" class="well-lg text-center"><?= \App\Message::message() ?></div>