<?php
/**
 * Created by PhpStorm.
 * User: Shuvo
 * Date: 12/14/2018
 * Time: 11:01 AM
 */

require_once '../../vendor/autoload.php';
if ( ! isset( $_SESSION ) ) {
	session_start();
}


if ( ! isset( $_SESSION['email'] ) && ! isset( $_COOKIE['user_email'] ) ) {

	\App\Message::message( 'please login' );
	\App\Utility::redirect( 'authentication.php' );

}
//if User click on a Remember me option then the if block is execute(Cookies) ,otherwise the else block is execute(Session)..
if ( isset( $_COOKIE['user_email'] ) ) {
	$jsonDecodedObj = json_decode( $_COOKIE['user_email'] );
	$userEmail      = $jsonDecodedObj->UserEmail;
} else {
	$userEmail = $_SESSION['email'];
}

?>

<h1>Welcome</h1>
<?= $userEmail ?>
<br>
<a class="btn btn-default" href="../logout.php">Log out</a>


