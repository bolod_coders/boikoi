


<?php
/**
 * Created by PhpStorm.
 * User: Shuvo
 * Date: 1/30/2019
 * Time: 2:15 AM
 */

require_once '../vendor/autoload.php';

   if (isset($_POST['search'])) {

       $response = '';
       $searchedBooks = new \App\Book();
       $searchedBooks = $searchedBooks->search_book($_POST['q']);
       $searchedBooks = json_decode(json_encode($searchedBooks), true);

       //\App\Utility::dd($searchedBooks);





           if ($searchedBooks > 0) {

               foreach ($searchedBooks as $searchedBook) {
                  $bookName =  $searchedBook['book_name'];
                  $bookId =  $searchedBook['book_id'];
                  $bookEn =  $searchedBook['book_name_en'];
                  $bookAuthor =  $searchedBook['author_name'];
                  $bookCover =  $searchedBook['book_cover'];


                   $response .= "<a href='bookDetails.php?book_id=$bookId'><ul class=''>";
                   
                   $response .= "<li><img src='img/books/$bookCover' alt='' class='img-fluid float-left pr-2' width='30px'></li>";
                   $response .= "<li class='float-right'>$bookAuthor</li>";
                   $response .= "<li class='float-left'>$bookName </li>";

                   $response .= "</ul></a>";

               }
           }

                if (empty($searchedBooks)){
                    $response .= "<ul ><li style='list-style: none; '>No data found </li></ul>";
                }







       exit($response);
   }

