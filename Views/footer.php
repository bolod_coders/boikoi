<footer id="footer" class="mt-3 bg-white shadow-sm rounded-top border-top">

    <div class="container">
        <div class="row small text-uppercase">
            <div class="col-md-3">

                <p class="mt-2"><i class="fas fa-phone mr-2"></i> Hotline <em>16297 / 015 1952 1971</em></p>
                <p class="mt-2"><i class="fab fa-sellsy mr-2"></i> Corporate Sales <em> 0181532654</em></p>
                <p class="mt-2"><i class="fas fa-envelope-open mr-2"></i> admin@boiache.com</p>
                <p class="mt-2"><i class="fas fa-map-marker-alt mr-2"></i> ChawkBazar, Chittagong </p>

            </div>
            <div class="col-md-2">
                <p class="font-weight-bold text-muted"> Shop By</p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">Authors</a></p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">Categories</a></p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">Publishers</a></p>
            </div>


            <div class="col-md-2">
                <p class="font-weight-bold text-muted"> Get To Know Us</p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">Contact Us</a></p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">About Us</a></p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">Blog</a></p>
            </div>


            <div class="col-md-2">
                <p class="font-weight-bold text-muted"> Policy</p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">Terms &
                        Conditions</a></p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">Privacy Policy</a>
                </p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">Refund Policy</a></p>
            </div>


            <div class="col-md-3">
                <p class="font-weight-bold text-muted"> Connect Us</p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">Facebook</a></p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">Twitter</a></p>
                <p class="pt-2"><i class="fas fa-angle-right mr-2"></i> <a href="" class="catLink">Youtube</a></p>
            </div>

        </div>
    </div>
</footer>


<!--jquery-ui-->
<script src="../Resource/js/bootstrap.min.js"></script>


<!--SearchBox filter-->
<script>
    function myFunction() {
        var input, filter, mainDiv, mainDivOfInput, span, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        mainDiv = document.getElementById("myUL");
        mainDivOfInput = mainDiv.getElementsByTagName("div");

        for (i = 0; i < mainDivOfInput.length; i++) {

            span = mainDivOfInput[i].getElementsByTagName("span")[0];
            txtValue = span.textContent || span.innerText;

            console.log(txtValue.toUpperCase().indexOf(filter));
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                mainDivOfInput[i].style.display = "";
            } else {
                mainDivOfInput[i].style.display = "none";
            }

        }
    }
</script>


<script>
    $('.carousel').carousel({
        interval: 4000
    })
</script>
<!--<noscript>
    $( function() {
    var availableTags = [
    "ActionScript",
    "AppleScript",
    "Asp",
    "BASIC",
    "C",
    "C++",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Erlang",
    "Fortran",
    "Groovy",
    "Haskell",
    "Java",
    "JavaScript",
    "Lisp",
    "Perl",
    "PHP",
    "Python",
    "Ruby",
    "Scala",
    "Scheme"
    ];
    $( "#tags" ).autocomplete({
    source: availableTags
    });
    } );
</noscript>-->


<!--<script>

    $(function () {

        $("#search").autocomplete({
            source: //"autocomplete.php",
                [
                    {
                    id: "74",
                    title: "Boi Mela",
                    author: "Humayon Ahmed",
                    label: "Humayon Ahmed",
                    img: "img/books/3b18ef9ea804_1143121547129022.gif"
                },
                    {
                        id: "75",
                        title: "Kichukkhon",
                        author: "Humayon Ahmed ",
                        label: "Humayon Ahmed",
                        img: "img/books/3b18ef9ea804_1143121547129022.gif"
                    },
                    {
                        id: "3",
                        title: "Aroj Ali Somipe",
                        author: "Jafor Ikbal",
                        label: "Jafor Ikbal",
                        img: "img/books/4ff1cf5e1_80221547059299.jpg"
                    }
                ],
            minLength: 1,
            select: function (event, ui) {
                /*
                 var url = ui.item.id;
                 if(url != '') {
                 location.href = '...' + url;
                 }
                 */
            },
            html: true,
            open: function (event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);

            }
        })
            .autocomplete("instance")._renderItem = function (ul, item) {
            return $("<a href='bookDetails.php?book_id=" + item.id + "'> <li class='p-2'><table width='100%'> <tr><td width='35px'><img src='" + item.img + "' width='35px'></td><td class='pl-2'> <span>" + item.title + "</span> <p class='text-muted'>" + item.author + "</p> </td> <td class='text-danger align-self-center text-right'>(15% off)</td> <td class='align-self-center text-right' width='15%'>90 Tk.</td> </tr> </table></li></a>").appendTo(ul);
        };

    });
</script>-->


<script>
    $('#searchID').keyup(function () {
        var query = $(this).val();


       if(query.length > 1){
            $.ajax({
                url:'searched.php',
                method:'POST',
                data :{
                    search:1,
                    q: query
                },
                success: function (data) {
                    $('#response').html(data)
                }


            })
        }

        if(query.length < 1){
            $('#response').html('');
        }


    })
</script>



<script>

    function readMore() {
        var visible = document.getElementsByClassName('showtxt')[0];
        visible.style.opacity = '1';


        var removeElm = document.getElementsByClassName('hideTag')[0];
        removeElm.style.display = 'none';

    }

</script>

<script>


    $('#message').fadeOut(500);
    $('#message').fadeIn(500);
    $('#message').fadeOut(500);
    $('#message').fadeIn(500);
    $('#message').fadeOut(1500);


</script>


<script>
    $(document).ready(function () {



        $('.radioSearch').click(function () {

            document.forms[1].action = "search.php";
            document.forms[1].submit();



        });

        $('.checkboxSearch').click(function () {

            document.forms[2].action = "search.php";
            document.forms[2].submit();




        });




    });

</script>




</body>
</html>