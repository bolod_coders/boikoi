<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/23/2018
 * Time: 10:31 PM
 */

include 'header.php';

require_once '../vendor/autoload.php';

$publishers = new \App\Publisher( null, null, null ,null);
$publishers = $publishers->showPublisherList();

$publishers = json_decode( json_encode( $publishers ), true );

?>

    <section class="container mt-3">
        <div class="bg-white border p-4 shadow-sm rounded">

            <h4 class="pb-4">Publishers
				<?php if ( isset( $_SESSION['email'] ) ) {


					if ( $person != false ) {
						/* Admin */
						echo "
                <a href='admin/addPublisher.php' class='btn btn-success float-right'>Add New Publisher</a>                                                                                    
                        ";

					}


				}
				?>
            </h4>


            <p class="-medium text-muted">সাহিত্যের আঁতুড়ঘর। সাহিত্যের সঠিক মূল্যায়ন করে পাঠকের কাছে উন্মুক্ত করে
                দেয়ার গুরুদায়িত্ব এই প্রকাশকদের। কেবল বই প্রকাশ নয়,ভালো মানের সাহিত্য বই আকারে নিয়মিত প্রকাশ করে
                প্রকাশকরা সাহিত্যের ধারা সচলভাবে অব্যাহত রাখেন। পাণ্ডুলিপি,ছাপা,মুদ্রণপ্রমাদ,সব বাঁধা-ত্যাগ-তিতিক্ষা পার
                করে যখন একটি বাঁধাই করা বই,এক টুকরো সাহিত্য ফসল পাঠকের সাহিত্য রসের ক্ষুধা পূরণ করে তখনই প্রকাশক সার্থক।
                প্রকাশকদের এই সাহিত্যধারার ট্রেনের সহযাত্রী হয়ে তাই রয়েছে রকমারি ডট কম। রকমারি ডট কম প্রকাশকদের সুযোগ
                করে দিচ্ছে আধুনিক সাহিত্যের রেনেসাঁর,যেখানে সাধারণ পাঠক আর প্রকাশকের মাঝে সেতুবন্ধন হিসেবে কাজ করবে
                রকমারি।</p>

            <!--////////////////////////////////Pagination Start //////////////////////////////////////////////-->

			<?php

			$pagination = new \App\Publisher( null, null, null ,null);

			if ( isset( $_GET['pageNo'] ) ) {
				$pageNo = $_GET['pageNo'];
			} else {
				$pageNo = 1;
			}
			$itemPerPage = 48;

			$offset = ( $pageNo - 1 ) * $itemPerPage;

			$totalPages = ceil( count( $publishers ) / $itemPerPage );

			$someRecords = $pagination->paginateActiveList( $offset, $itemPerPage );

			$someRecords = json_decode( json_encode( $someRecords ), true );


			?>

            <div class="row mt-4">
                <div class="col-6">
                    <nav class="nav">

                        <ul class="pagination">
                            <li class="page-item <?php if ( $pageNo == 1 ) {
								echo 'disabled';
							} ?>"><a href="?pageNo=1" class="page-link">First</a></li>
                            <li class="<?php if ( $pageNo <= 1 ) {
								echo 'disabled';
							} ?> page-item">
                                <a href="<?php if ( $pageNo <= 1 ) {
									echo '#';
								} else {
									echo "?pageNo=" . ( $pageNo - 1 );
								} ?>" class="page-link">Prev</a>
                            </li>
                            <li class="page-item <?php if ( $pageNo >= $totalPages ) {
								echo 'disabled';
							} ?>">
                                <a href="<?php if ( $pageNo >= $totalPages ) {
									echo '#';
								} else {
									echo "?pageNo=" . ( $pageNo + 1 );
								} ?>" class="page-link">Next</a>
                            </li>
                            <li class="page-item <?php if ( $pageNo == $totalPages ) {
								echo 'disabled';
							} ?>"><a href="?pageNo=<?php echo $totalPages; ?>" class="page-link">Last</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-6">
                    <form class="form-inline float-right">
                        <input class="form-control mr-sm-4 pr-5 " type="search" placeholder="Search Publisher"
                               aria-label="Search">
                        <button class="btn  my-2 my-sm-0 btn-success" type="submit"><i class="fas fa-search"></i>
                        </button>
                    </form>
                </div>
            </div>

            <!--////////////////////////////////Pagination End //////////////////////////////////////////////-->

            <div class="row mt-3 text-center ">

				<?php

				foreach ( $someRecords as $publisher ) {
					$publisherId    = $publisher["publisher_id"];
					$publisherName  = $publisher["publisher_name"];
					$publisherImage = $publisher["publisher_logo"];

					if ( $publisherImage == null || $publisherImage == "" ) {
						$publisherImage = "publisher.png";
					}

					echo "
	            
            <div class='col-md-2 pb-4'>
                <a href='search.php?publisherId=$publisherId' class='catLink'>
                    <img class='img-fluid rounded-circle w-40' src='img/publishers/$publisherImage' alt='$publisherName'>
                    <p class='mt-2'>$publisherName</p>
                </a>  
            </div>
	            
	            ";
				}

				?>


            </div>


        </div>

    </section>


<?php
include 'footer.php';
?>