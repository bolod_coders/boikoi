<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/15/2018
 * Time: 8:34 PM
 */

require_once '../vendor/autoload.php';
if ( ! isset( $_SESSION ) ) {
	session_start();
}

if ( isset( $_COOKIE['user_email'] ) ) {
	unset( $_COOKIE['user_email'] );
	setcookie( 'user_email', "", - 1000 );
}


if ( isset( $_SESSION['email'] ) ) {
	\App\Message::message( "Logout successfully. Please Login again. " );
	unset( $_SESSION['email'] );
}


header( 'Location:authentication.php' );