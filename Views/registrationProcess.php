<?php
/**
 * Created by PhpStorm.
 * User: Shuvo
 * Date: 12/6/2018
 * Time: 11:01 PM
 */

require_once '../vendor/autoload.php';

$name        = $_POST['fullName'];
$email       = $_POST['email'];
$password    = $_POST['password'];
$confirmPass = $_POST['confirm_password'];

if ( isset( $_POST['remember'] ) ) {
	$remember = $_POST['remember'];
}

$passwordHash = password_hash( $password, PASSWORD_BCRYPT );


$person   = new \App\Person( $name, $email, $passwordHash );
$emailobj = $person->emailMatching( $email );

// $emailobj is an object we want to convert this to string but failed .(forkan)...


if ( $emailobj == false ) {

	if ( $_POST["password"] !== $_POST["confirm_password"] ) {

		App\Message::message( "password not matched" );

		\App\Utility::redirect( 'authentication.php?tab=signup' );

	} else {

		$person->insertRegisterUser();

		$emailSendLink = 'email.php?email=' . $person->getEmail() . '&hash=' . $person->getPassword() . "&fullName=" . $person->getName();

		\App\Utility::redirect( $emailSendLink );

	}

} else {

	\App\Message::message( 'Email is already registered' );
	\App\Utility::redirect( 'authentication.php?tab=signup' );

}




/*  1 - PasswordHashing and insert into database
    2 - checking email is already exits in database or not
    3 -  think  easy way to solve the problem & check  the oposite return too


    */











