<?php
/**
 * Created by PhpStorm.
 * User: Forkan
 * Date: 1/6/2019
 * Time: 10:18 PM
 */

require_once '../vendor/autoload.php';
include_once 'header.php';

$book_id = $_GET["book_id"];

$books = new \App\Book();
$books->setId( $book_id );
$singleBook = $books->view_book();
$singleBook = json_decode( json_encode( $singleBook ), true );


/*
    Book
*/
$bookId     = $singleBook["book_id"];
$bookName   = $singleBook["book_name"];
$bookNameEn = $singleBook["book_name_en"];

$bookPrice    = $singleBook["book_price"];
$bookISBN     = $singleBook["book_isbn"];
$bookEdition  = $singleBook["book_edition"];
$bookPage     = $singleBook["book_page"];
$bookStock    = $singleBook["book_in_stock"];
$bookLanguage = $singleBook["book_language"];
$bookIsOff    = $singleBook["book_is_off"];
$bookCover    = $singleBook["book_cover"];
$bookSummary  = $singleBook["book_summary"];
$bookCountry  = $singleBook["book_country"];
$bookLanguage = $singleBook["book_language"];
$bookType     = $singleBook["book_type"];

/*
    Author
*/
$authorID      = $singleBook["author_id"];
$bookAuthor    = $singleBook["author_name"];
$authorDetails = $singleBook["author_details"];
$authorImage   = $singleBook["author_image"];

/*
    Catgory
*/
$categoryID   = $singleBook["category_id"];
$bookCategory = $singleBook["category_name"];

/*
    Publisher
*/
$publisherID   = $singleBook["publisher_id"];
$bookPublisher = $singleBook["publisher_name"];


if ( $bookCover == "" || $bookCover == null ) {
	$bookCover = "default.png";
}


$hideOff           = '';
$bookPriceAfterOff = '';
if ( $bookIsOff > 0 ) {
	$bookPriceAfterOff = round( $bookPrice - ( ( $bookPrice * $bookIsOff ) / 100 ) );
} else {
	$hideOff = "hideOff";
}


?>


<section class="container">
    <div class="bg-white mt-3 mb-3 shadow-sm rounded p-4">
        <div class="row">
            <div class="col-3">
                <img src="img/books/<?= $bookCover ?>" alt="<?= $bookName ?>" class="img-fluid border p-2 w-auto">
            </div>
            <div class="col-9 align-self-center">
                <h1 class="h3 mb-3"><?= $bookName ?><?php if ( $bookType != null & $bookType != "" ) {
						echo " (" . $bookType . ")";
					} ?></h1>
                <p class="authorTitle mb-3"><a href="search.php?authorID=<?= $authorID ?>"><?= $bookAuthor ?></a></p>
                <span class="h5">TK : <?php if ( $bookIsOff > 0 ) {
						echo $bookPriceAfterOff . " <del class='text-muted ml-2' > $bookPrice </del>";
					} else {
						echo $bookPrice;
					} ?></span> <span class="<?= $hideOff ?> bookOff mt-2">&nbsp;<?= $bookIsOff ?>% Off</span>

                <p class="mt-3"> Category :<a href="search.php?categoryID=<?= $categoryID ?>"> <?= $bookCategory ?></a>
                </p>
                <button class="btn btn-success mt-3 mr-2">Add to Cart</button>


				<?php if ( isset( $_SESSION['email'] ) ) {


					if ( $person != false ) {
						/* Admin */
						echo "
                <a href='admin/editBook.php?book_id=$bookId' class='btn btn-outline-dark mt-3'>Edit</a>                                                                                    
                        ";

					}


				}
				?>


            </div>
        </div>
        <div class="border-bottom mt-3"></div>
        <div class="row mt-4">

            <h4 class="ml-3 mb-3">Product Specification & Summary</h4>


            <div class="col-md-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                           aria-controls="home" aria-selected="true">Specification</a>
                    </li>

					<?php

					if ( $bookSummary != null || $bookSummary != "" ) {
						echo "
                            <li class='nav-item'>
                                <a class='nav-link' id='profile-tab' data-toggle='tab' href='#profile' role='tab'
                                   aria-controls='profile' aria-selected='false'>Summary</a>
                            </li>
                        ";
					}

					?>


                </ul>
            </div>
            <div class="tab-content col-md-12" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class=" mt-3 ">
                        <table class="table ">
                            <tbody>

							<?php
							if ( $bookName != null || $bookName != "" ) {
								echo "
                                    <tr>
                                        <th scope='row' width='15%' class='table-active'>Name</th>
                                        <td> $bookName </td>
                                    </tr>
                                    ";
							}

							if ( $bookAuthor != null || $bookAuthor != "" ) {
								echo "
                                    <tr>
                                        <th scope='row' width='15%' class='table-active'>Author</th>
                                        <td> <a href='search.php?authorID=$authorID'> $bookAuthor </a></td>
                                    </tr>
                                    ";
							}

							if ( $bookPublisher != null || $bookPublisher != "" ) {
								echo "
                                    <tr>
                                        <th scope='row' width='15%' class='table-active'>Publisher</th>
                                        <td> <a href='search.php?publisherID=$publisherID'> $bookPublisher </a> </td>
                                    </tr>
                                    ";
							}

							if ( $bookISBN != null || $bookISBN != "" ) {
								echo "
                                    <tr>
                                        <th scope='row' width='15%' class='table-active'>ISBN</th>
                                        <td> $bookISBN </td>
                                    </tr>
                                    ";
							}

							if ( $bookEdition != null || $bookEdition != "" ) {
								echo "
                                    <tr>
                                        <th scope='row' width='15%' class='table-active'>Edition</th>
                                        <td> $bookEdition </td>
                                    </tr>
                                    ";
							}

							if ( $bookPage != null || $bookPage != "" ) {
								echo "
                                    <tr>
                                        <th scope='row' width='15%' class='table-active'>Number of Pages</th>
                                        <td> $bookPage </td>
                                    </tr>
                                    ";
							}

							if ( $bookCountry != null || $bookCountry != "" ) {
								echo "
                                    <tr>
                                        <th scope='row' width='15%' class='table-active'>Country</th>
                                        <td> $bookCountry </td>
                                    </tr>
                                    ";
							}

							if ( $bookLanguage != null || $bookLanguage != "" ) {
								echo "
                                    <tr>
                                        <th scope='row' width='15%' class='table-active'>Language</th>
                                        <td> $bookLanguage </td>
                                    </tr>
                                    ";
							}
							?>


                            </tbody>
                        </table>
                    </div>
                </div>

				<?php

				if ( $bookSummary != null || $bookSummary != "" ) {
					echo "
                        <img src='img/authors/$bookCover' alt='' class='img-fluid'>
                        <div class='tab-pane fade mt-3 pb-3' id='profile' role='tabpanel' aria-labelledby='profile-tab'>
                              $bookSummary     
                        </div>
                    ";
				}

				?>

            </div>


        </div>

    </div>
</section>


<section class="container">
    <div class="bg-white mt-3 mb-3 shadow-sm rounded p-4">
        <h4 class="pt-3">Author Information</h4>
        <div class="row p-4">

            <div class="col-2">
				<?php
				if ( $authorImage == null || $authorImage == "" ) {
					$authorImage = "author.png";
				}
				?>
                <img src="img/authors/<?= $authorImage ?>" alt="" class="img-fluid rounded-circle">
            </div>
            <div class="col-10">
                <h5><a href='search.php?authorID=$authorID' class="catLink"><?= $bookAuthor ?></a></h5>
				<?php

				if ( $authorDetails != null || $authorDetails != "" ) {
					echo "<span>" . substr( "$authorDetails", 0, 1000 )
					     . "<span style='cursor: pointer' class='text-success hideTag' onclick='readMore()'>.....Read more</span>"
					     . "<span style='opacity: 0;'  class='showtxt'>" . substr( "$authorDetails", 1000 ) . "</span>"
					     . "</span>";
				}

				?>
            </div>

        </div>
    </div>
</section>


<?php
include_once 'footer.php';
?>
