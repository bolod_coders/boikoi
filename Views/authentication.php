<?php

require_once '../vendor/autoload.php';

if ( ! isset( $_SESSION ) ) {
	session_start();
}

if ( isset( $_SESSION['email'] ) ) {
	\App\Utility::redirect( 'index.php' );
}

$db     = new \App\Database();
$active = "";
if ( isset( $_GET["tab"] ) ) {
	$active = "active";

}

include "header.php";

?>

    <section>


        <div class="container border mt-3 bg-white">

            <div class="row">

                <div class="col-md-6 mt-5 p-5">
                    <img src="img/authCover.jpg" alt="" class="img-fluid">
                </div>
                <div class="col-md-5 mt-5">

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item ">
                            <a class="nav-link <?php if ( ! isset( $_GET["tab"] ) )
								echo "active" ?>" id="login-tab"
                               data-toggle="tab" href="#login" role="tab"
                               aria-controls="login" aria-selected="true">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?= $active ?>" id="signup-tab" data-toggle="tab" href="#signup"
                               role="tab"
                               aria-controls="signup" aria-selected="false">Sign Up</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="myTabContent">

                        <div class="text-center">
							<?php
							echo \App\Message::message();
							?>
                        </div>

                        <div class="tab-pane fade show <?php if ( ! isset( $_GET["tab"] ) )
							echo "active" ?>" id="login"
                             role="tabpanel" aria-labelledby="login-tab">

                            <form action="loginProcess.php" method="post">
                                <div class="form-group row mt-5">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email"
                                               name="email"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="inputPassword3"
                                               placeholder="Password" name="password" required>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck1"
                                                   name="remember">
                                            <label class="form-check-label" for="gridCheck1">
                                                &nbsp; Remember password
                                            </label>
                                            <label class="form-check-label float-right" for="gridCheck1">
                                                <a href="forgotPassword.php">Forgot Password?</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary auth_btn">Sign in</button>
                                    </div>
                                </div>
                            </form>

                        </div>


                        <div class="tab-pane fade show <?= $active ?>" id="signup" role="tabpanel"
                             aria-labelledby="signup-tab">

                            <form action="registrationProcess.php" method="post">


                                <div class="form-group row mt-5">
                                    <label for="inputFullName" class="col-sm-3 col-form-label">Full Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="inputFullName"
                                               placeholder="Full Name" name="fullName" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email"
                                               name="email" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-3 col-form-label">Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="inputPassword3"
                                               placeholder="Password" name="password" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-3 col-form-label">Confirm Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="inputPassword3"
                                               placeholder="Confirm Password" name="confirm_password" required>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-primary auth_btn">Sign Up</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>


        </div>

    </section>


<?php
include "footer.php";
?>