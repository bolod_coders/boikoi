<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/23/2018
 * Time: 10:31 PM
 */


include 'header.php';

require_once '../vendor/autoload.php';

$categories = new \App\Category( null, null,null);
$categories = $categories->showCategoryList();

$categories = json_decode( json_encode( $categories ), true );

?>

    <section class="container mt-3">
        <div class="bg-white border p-4 shadow-sm rounded">

            <h4 class="pb-4">Categories
				<?php if ( isset( $_SESSION['email'] ) ) {


					if ( $person != false ) {
						/* Admin */
						echo "
                <a href='admin/addCategory.php' class='btn btn-success float-right'>Add New Category</a>                                                                                    
                        ";

					}


				}
				?>
            </h4>


            <p class="-medium text-muted">লক্ষাধিক বইয়ের সংগ্রহ রকমারি ডট কমে। বইয়ের এই বিশাল সমুদ্র-মন্থনে পাঠকের
                সুবিধার্থে প্রায় ৫০ টির মতো ক্যাটাগরি ও সহস্রাধিক বিষয়ভিত্তিক ক্যাটাগরি রয়েছে রকমারি ডট কমে। যার ফলে খুব
                সহজেই পাঠক তার পছন্দের ক্যাটাগরি বেছে নিয়ে নির্দিষ্ট বিষয়ভিত্তিক বইগুলো খুঁজে পাবে খুব সহজেই। রকমারি ডট
                কমের এই বিশাল বইয়ের সমুদ্রে জ্ঞানের জাহাজের নাবিক হতে আপনাকে নিমন্ত্রণ। মানচিত্রটা ধরা আছে নিচেই...</p>

            <!--////////////////////////////////Pagination Start //////////////////////////////////////////////-->

			<?php

			$pagination = new \App\Category( null, null,null );

			if ( isset( $_GET['pageNo'] ) ) {
				$pageNo = $_GET['pageNo'];
			} else {
				$pageNo = 1;
			}
			$itemPerPage = 48;

			$offset = ( $pageNo - 1 ) * $itemPerPage;

			$totalPages = ceil( count( $categories ) / $itemPerPage );

			$someRecords = $pagination->paginateActiveList( $offset, $itemPerPage );

			$someRecords = json_decode( json_encode( $someRecords ), true );


			?>

            <div class="row mt-4">
                <div class="col-6">
                    <nav class="nav">

                        <ul class="pagination">
                            <li class="page-item <?php if ( $pageNo == 1 ) {
								echo 'disabled';
							} ?>"><a href="?pageNo=1" class="page-link">First</a></li>
                            <li class="<?php if ( $pageNo <= 1 ) {
								echo 'disabled';
							} ?> page-item">
                                <a href="<?php if ( $pageNo <= 1 ) {
									echo '#';
								} else {
									echo "?pageNo=" . ( $pageNo - 1 );
								} ?>" class="page-link">Prev</a>
                            </li>
                            <li class="page-item <?php if ( $pageNo >= $totalPages ) {
								echo 'disabled';
							} ?>">
                                <a href="<?php if ( $pageNo >= $totalPages ) {
									echo '#';
								} else {
									echo "?pageNo=" . ( $pageNo + 1 );
								} ?>" class="page-link">Next</a>
                            </li>
                            <li class="page-item <?php if ( $pageNo == $totalPages ) {
								echo 'disabled';
							} ?>"><a href="?pageNo=<?php echo $totalPages; ?>" class="page-link">Last</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-6">
                    <form class="form-inline float-right">
                        <input class="form-control mr-sm-4 pr-5 " type="search" placeholder="Search Category"
                               aria-label="Search">
                        <button class="btn  my-2 my-sm-0 btn-success" type="submit"><i class="fas fa-search"></i>
                        </button>
                    </form>
                </div>
            </div>

            <!--////////////////////////////////Pagination End //////////////////////////////////////////////-->

            <div class="row mt-3 text-center ">

				<?php

				foreach ( $someRecords as $category ) {
					$categoryId   = $category["category_id"];
					$categoryName = $category["category_name"];
					echo "
	            
            <div class='col-md-2 pb-4'>
                <a href='search.php?categoryId=$categoryId' class='catLink'>
                    <img class='img-fluid rounded-circle w-40' src='img/categories/category.png' alt='$categoryName'>
                
                <p class='mt-2'>$categoryName</p>
                </a>  
            </div>
	            
	            ";
				}

				?>


            </div>


        </div>

    </section>


<?php
include 'footer.php';
?>