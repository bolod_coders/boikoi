<?php
/**
 * Created by PhpStorm.
 * User: Shuvo
 * Date: 12/14/2018
 * Time: 8:55 AM
 */
if ( ! isset( $_SESSION ) ) {
	session_start();
}

require_once '../vendor/autoload.php';
$person = new \App\Person( null, null, null );


$email    = $_POST['email'];   //shuvoahmedkhan202@gmail.com
$password = $_POST['password'];//123

$dbPerson = (array) $person->loginVerify( $email );

$dbEmail        = $dbPerson['user_email'];
$dbPassword     = $dbPerson['user_password'];
$verifyPassword = password_verify( $password, $dbPassword );

if ( $email == $dbEmail && $verifyPassword ) {

	$_SESSION['email'] = $email;

	if ( isset( $_POST['remember'] ) ) {

		$cookieData     = [ "UserEmail" => $email, "Expire" => time() + 30 * 24 * 3600 ];
		$jsonEncodedStr = json_encode( $cookieData );
		setcookie( "user_email", $jsonEncodedStr, $cookieData["Expire"] );
		\App\Utility::redirect( 'index.php' );

	} else {
		setcookie( "user_email", "", time() - 1000 );
		\App\Utility::redirect( 'index.php' );
	}


} else {
	\App\Message::message( 'login failed' );
	\App\Utility::redirect( 'authentication.php' );
}


/*
 * login auth end
 * session start
 * cookies start
 *
 * */



