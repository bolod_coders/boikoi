<?php

include_once 'header.php';




if (isset($_GET['search'])){
    $searchedBooks = new \App\Book();
    $searchedBooks = $searchedBooks->search_book( $_GET['search'] );
    $searchedBooks = json_decode( json_encode( $searchedBooks ), true );
}


if(isset($_GET['searchNew'])){
    $searchedBooks = new \App\Book();
    $searchedBooks = $searchedBooks->new_realeased();
    $searchedBooks = json_decode( json_encode( $searchedBooks ), true );



}
if(isset($_GET['priceLowToHigh'])){
    $searchedBooks = new \App\Book();
    $searchedBooks = $searchedBooks->price_low_high();
    $searchedBooks = json_decode( json_encode( $searchedBooks ), true );




}

if(isset($_GET['priceHighToLow'])){
    $searchedBooks = new \App\Book();
    $searchedBooks = $searchedBooks->price_high_low();
    $searchedBooks = json_decode( json_encode( $searchedBooks ), true );

}

if(isset($_GET['discountLowToHigh'])){
    $searchedBooks = new \App\Book();
    $searchedBooks = $searchedBooks->discount_low_high();
    $searchedBooks = json_decode( json_encode( $searchedBooks ), true );

}


if(isset($_GET['discountHighToLow'])){
    $searchedBooks = new \App\Book();
    $searchedBooks = $searchedBooks->discount_high_low();
    $searchedBooks = json_decode( json_encode( $searchedBooks ), true );


}


$categories = new \App\Category(null,null,null);
$categories = $categories->showCategoryList();
$categories = json_decode( json_encode( $categories ), true );




if (isset($_GET["category"])){
    $searchedbooks = implode(',',$_GET['category']);
    $searchedBooks = new \App\Book();
    $searchedBooks = $searchedBooks->category_list($searchedbooks);
    $searchedBooks = json_decode( json_encode( $searchedBooks ), true );


}



// As output of $_POST['Color'] is an array we have to use foreach Loop to display individual value




?>




<section id="searchItems">
    <div class="container">
        <div class="row mt-3">
            <div class="col-md-3">

                <!-- Sort Radio buttons-->
                <form action="#" class="mb-3 bg-white shadow-sm rounded" method="get" name="frm1">


                    <div class="border">
                        <h4 class="border-bottom p-3"> Sort</h4>
                        <div class="p-3">


                            <div class="pb-1">
                                <input type="radio" id="newReleased" name="searchNew" <?php if(isset($_GET['searchNew'])){ echo 'checked';}  ?> value="ID_DESC" class="radioSearch">
                                <label for="newReleased">New Released</label>
                            </div>

                            <div class="pb-1">
                                <input type="radio" id="priceLowToHigh" name="priceLowToHigh" <?php if(isset($_GET['priceLowToHigh'])){ echo 'checked';}  ?> value="PRICE_ASC" class="radioSearch">
                                <label for="priceLowToHigh">Price - Low to High</label>
                            </div>
                            <div class="pb-1">
                                <input type="radio" id="test5" name="priceHighToLow" <?php if(isset($_GET['priceHighToLow'])){ echo 'checked';}  ?> value="PRICE_DESC" class="radioSearch">
                                <label for="test5">Price - High to Low</label>
                            </div>
                            <div class="pb-1">
                                <input type="radio" id="test6" name="discountLowToHigh" <?php if(isset($_GET['discountLowToHigh'])){ echo 'checked';}  ?> value="DISCOUNT_ASC" class="radioSearch">
                                <label for="test6">Discount - Low to High</label>
                            </div>
                            <div class="pb-1">
                                <input type="radio" id="test7" name="discountHighToLow" <?php if(isset($_GET['discountHighToLow'])){ echo 'checked';}  ?> value="DISCOUNT_DESC" class="radioSearch">
                                <label for="test7">Discount - High to Low</label>
                            </div>

                        </div>
                    </div>
                </form>


                <!--Authors checkbox-->
                <form action="" class="mb-3 bg-white shadow-sm rounded" method="get" name="filterForm">
                    <div class="border mb-3 ">
                        <h4 class="border-bottom p-3">Categories</h4>
                        <div class="text-center" align="center">
                            <input type="text" id="myInput" onkeyup="myFunction()"
                                   placeholder="&nbsp; Search for names.." title="Type in a name"
                                   class=" mt-2 form-control" style="width: 220px; margin-left: 14px">
                        </div>
                         <div class="p-1 pt-2 scroll" id="myUL">



                            <?php

                                /*foreach ($_GET['category'] as $selected){
                                    $selected = $categoryId;
                                }*/


                            foreach ($categories as $category){
                                $categoryName = $category['category_name'];
                                $categoryId = $category['category_id'];

                                $checked = '';
                                if(isset($_GET['category'])== $categoryId)  $checked = 'checked';


                                echo "

                                          <div class='checkboxTwo'>
                                <input type='checkbox'  id='$categoryId'   name='category[]' $checked  value='$categoryId' class='checkboxSearch' $checked />
                                <label for='$categoryId'></label>
                                <span class='r'>$categoryName </span>
                            </div>
                                
                                ";


                            }

                            ?>





                        </div>
                    </div>
                </form>


                <!--price Range slider-->
                <form action="" class="bg-white shadow-sm rounded">
                    <div class="border ">
                        <h4 class="border-bottom p-3">Price</h4>
                        <div class="p-2 pt-2 pb-3 pr-3 pl-3 ">

                            <p class="mb-2">

                                <input type="text" id="amount" readonly style="border:0; color:black; font-weight: bold"
                                       class="text-center">
                            </p>

                            <div id="slider-range" style="padding: 0px"></div>

                        </div>
                    </div>
                </form>


            </div>


            <div class="col-md-9 items">
                <div class=" border bg-white p-3 shadow-sm rounded">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Search</li>
                        </ol>
                    </nav>
                    <p><?= sizeof( $searchedBooks)?> books found</p>

                    <div class="row pt-4 p-2">

						<?php

						if ( sizeof( $searchedBooks ) > 0 ) {


							foreach ( $searchedBooks as $searchedBook ) {
								$latestBookId    = $searchedBook["book_id"];
								$latestBookCover = $searchedBook["book_cover"];
								if ( $latestBookCover == "" || $latestBookCover == null ) {
									$latestBookCover = "default.png";
								}
								$latestBookName   = $searchedBook["book_name"];
								$latestBookAuthor = $searchedBook["author_name"];
								$latestBookPrice  = $searchedBook["book_price"];
								$latestBookOff    = $searchedBook["book_is_off"];
								$hideOff          = "";

								if ( $latestBookOff > 0 ) {
									$latestBookPrice = round( $latestBookPrice - ( ( $latestBookPrice * $latestBookOff ) / 100 ) );
								} else {
									$hideOff = "hideOff";
								}

								echo "
	            
                            <div class='col pb-4 text-center p-2' style='max-width: 160px'>
    
                                <img class='img-fluid' src='img/books/$latestBookCover' alt='$latestBookName'>
    
                                <p class='mt-2 bookTitle'>$latestBookName</p>
                                <p class='mt-2 authorTitle'>$latestBookAuthor</p>
                                <span class='mt-2 bookPrice'><b>TK. $latestBookPrice </b></span> <span
                                        class='$hideOff bookOff'>($latestBookOff% off)</span>
    
    
                                <div class='addToCartBtn'>
                                    <a href='bookDetails.php?book_id=$latestBookId' class='overlay text-center'>
    
                                        <a href='viewProfile.html' class='btn btn-info showHideCart'>Add to Cart</a>
    
                                    </a>
                                </div>
                            </div>
	            

	            
	            ";
							}

						}


						?>


                    </div>

                </div>
            </div>
        </div>
</section>




<?php
include_once 'footer.php';
?>
