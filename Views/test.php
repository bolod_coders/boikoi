<?php
include "header.php";
?>
    <style>
        .hideContent {
            overflow: hidden;
            line-height: 1em;
            height: 2em;
        }

        .showContent {
            line-height: 1em;
            height: auto;
        }

        .overlayNew {
            width: 60%;
            padding: 15px 0;
            transition: all 0.7s;
            margin: 0 auto;

        }

        .overlayNew:hover {
            background-color: rgba(251, 251, 251, 0.30);
            box-shadow: inset 0 0 7px 0 #cdcdcd;

        }

        .button {

            color: #dedede;
            background-color: rgb(131, 184, 102);
            width: 100%;

        }

        .buttonView {

            color: #dedede;
            background-color: rgb(56, 132, 184);
            width: 84%;

        }

        .product-image9 > a:hover {
            color: black;
        }

        .product-content > a:hover {
            color: black;
        }

        h3.h3 {
            text-align: center;
            margin: 1em;
            text-transform: capitalize;
            font-size: 1.7em;
        }

        /********************* Shopping Demo-9 **********************/
        .product-grid9, .product-grid9 .product-image9 {
            position: relative
        }

        .product-grid9 {
            z-index: 1
        }

        .product-grid9 .product-image9 a {
            display: block
        }

        .product-grid9 .product-image9 img {
            width: 130px;
            height: auto;
        }

        .product-grid9 .pic-1 {
            opacity: 1;
            transition: all .5s ease-out 0s
        }

        /*.product-grid9:hover .pic-1 {*/
        /*opacity: 0*/
        /*}*/

        .product-grid9 .pic-2 {
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
            transition: all .5s ease-out 0s
        }

        .product-grid9:hover .pic-2 {
            opacity: 1
        }

        .product-grid9 .product-full-view {
            opacity: 0;
            position: absolute;
            top: 50%;

            transition: all .3s ease 0s;

        }

        /*        .product-grid9 .product-full-view:hover {
					color: #c0392b
				}*/

        .product-grid9:hover .product-full-view {
            opacity: 1
        }

        .product-grid9 .product-content {
            padding: 12px 12px 0;
            overflow: hidden;
            position: relative
        }

        .product-content .rating {
            padding: 0;
            margin: 0 0 7px;
            list-style: none
        }

        .product-grid9 .rating li {
            font-size: 12px;
            color: #ffd200;
            transition: all .3s ease 0s
        }

        .product-grid9 .rating li.disable {
            color: rgba(0, 0, 0, .2)
        }

        .product-grid9 .title {
            font-size: 16px;
            font-weight: 400;
            text-transform: capitalize;
            margin: 0 0 3px;
            transition: all .3s ease 0s
        }

        .product-grid9 .title a {
            color: black;
        }

        .product-grid9 .title a:hover {
            color: #c0392b
        }

        .product-grid9 .price {
            color: #000;
            font-size: 17px;
            margin: 0;
            display: block;
            transition: all .5s ease 0s
        }

        .product-grid9:hover .price {
            opacity: 0
        }

        .product-grid9 .add-to-cart {
            opacity: 0;
            position: absolute;
            bottom: -20px;
            transition: all .5s ease 0s

        }

        .product-grid9:hover .add-to-cart {
            opacity: 1;
            bottom: 0
        }

        @media only screen and (max-width: 990px) {
            .product-grid9 {
                margin-bottom: 30px
            }
        }

    </style>


    <div class="container">
        <h3 class="h3">shopping Demo-9 </h3>
        <div class="row">
            <div class="col-md-3 ">
                <div class="product-grid9 overlayNew">
                    <div class="product-image9 text-center">
                        <div>
                            <a href="bookDetails.php?book_id=3" class="">
                                <img class="pic-1 img-fluid w-75" src="img/books/923a19a70434_8901547136022.gif">
                            </a>
                        </div>

                        <div>
                            <a href="#" class="fa fa-shopping-cart product-full-view button p-2"> Add to Cart</a>
                        </div>
                    </div>
                    <div class="product-content text-center">
                        <h3 class="title">হিজিবিজি</h3>
                        <div class="author text-muted">হুমায়ূন আহমেদ</div>
                        <div class="price mt-3"><b>TK. 213</b> <span class="text-danger small">(15% off)</span></div>
                        <a class="add-to-cart btn btn-block buttonView" href="bookDetails.php?book_id=3">View
                            Details</a>
                    </div>
                </div>
            </div>

        </div>
    </div>


<?php
include "footer.php";
?>