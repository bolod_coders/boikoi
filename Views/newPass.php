<?php
require_once '../vendor/autoload.php';

$email = $_GET['email'];

include 'header.php';
?>

    <section>


        <div class="container border mt-3 bg-white">

            <div class="row">

                <div class="col-md-6 mt-5 p-5">
                    <img src="img/authCover.jpg" alt="" class="img-fluid">
                </div>

                <div class="col-md-5 mt-5">
                    <form action="setPassword.php?email=<?= $email ?>" method="post">


                        <div class="form-group">
                            <label for="exampleInputEmail1">Input your new Password </label>
                            <input type="password" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   placeholder="Enter password" name="newpass">

                        </div>


                        <button type="submit" class="btn btn-primary" name="submit">Set password</button>


                    </form>
                </div>
            </div>
        </div>
    </section>


<?php
include 'footer.php';
?>