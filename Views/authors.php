<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/23/2018
 * Time: 8:56 PM
 */

include 'header.php';

require_once '../vendor/autoload.php';

$authors = new \App\Author( null, null, null, null,null );
$authors = $authors->showAuthorList();

$authors = json_decode( json_encode( $authors ), true );

?>

    <section class="container mt-3">

        <div class="bg-white border p-4 shadow-sm rounded">

            <h4 class="pb-3">Authors

				<?php if ( isset( $_SESSION['email'] ) ) {


					if ( $person != false ) {
						/* Admin */
						echo "
                <a href='admin/addAuthor.php' class='btn btn-success float-right'>Add New Author</a>                                                                                    
                        ";

					}


				}
				?>
            </h4>

            <p class="-medium text-muted">লেখক! আক্ষরিক ভাবে বলতে গেলে সৃজনশীল কোনকিছু লেখেন যিনি তাকেই লেখক বলা
                যায়। কিন্তু ‘লেখক’ শব্দটির
                ব্যাপ্তি আসলে এতোটুকুতেই সীমাবদ্ধ নয়। লেখক এই বাস্তবিক জগতের সমান্তরালে একটি কাল্পনিক পৃথিবী তৈরির
                ক্ষমতা রাখেন। কাল্পনিক চরিত্রগুলো তার লেখনী ও কলমের প্রাণখোঁচায় জীবন্ত হয়ে ওঠে। একজন লেখক তাঁর লেখার
                মাধ্যমে একটি প্রজন্মের চিন্তাধারা গড়ে দিতে পারেন। তাই লেখকদের কিংবদন্তী হবার পথ করে দিতে রকমারি ডট কম
                বদ্ধ পরিকর।</p>

            <!--////////////////////////////////Pagination Start //////////////////////////////////////////////-->

			<?php

			$pagination = new \App\Author( null, null, null, null,null );

			if ( isset( $_GET['pageNo'] ) ) {
				$pageNo = $_GET['pageNo'];
			} else {
				$pageNo = 1;
			}
			$itemPerPage = 48;

			$offset = ( $pageNo - 1 ) * $itemPerPage;

			$totalPages = ceil( count( $authors ) / $itemPerPage );

			$someRecords = $pagination->paginateActiveList( $offset, $itemPerPage );

			$someRecords = json_decode( json_encode( $someRecords ), true );


			?>

            <div class="row mt-4">
                <div class="col-6">
                    <nav class="nav">

                        <ul class="pagination">
                            <li class="page-item <?php if ( $pageNo == 1 ) {
								echo 'disabled';
							} ?>"><a href="?pageNo=1" class="page-link">First</a></li>
                            <li class="<?php if ( $pageNo <= 1 ) {
								echo 'disabled';
							} ?> page-item">
                                <a href="<?php if ( $pageNo <= 1 ) {
									echo '#';
								} else {
									echo "?pageNo=" . ( $pageNo - 1 );
								} ?>" class="page-link">Prev</a>
                            </li>
                            <li class="page-item <?php if ( $pageNo >= $totalPages ) {
								echo 'disabled';
							} ?>">
                                <a href="<?php if ( $pageNo >= $totalPages ) {
									echo '#';
								} else {
									echo "?pageNo=" . ( $pageNo + 1 );
								} ?>" class="page-link">Next</a>
                            </li>
                            <li class="page-item <?php if ( $pageNo == $totalPages ) {
								echo 'disabled';
							} ?>"><a href="?pageNo=<?php echo $totalPages; ?>" class="page-link">Last</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-6">
                    <form class="form-inline float-right">
                        <input class="form-control mr-sm-4 pr-5 " type="search" placeholder="Search Author"
                               aria-label="Search">
                        <button class="btn  my-2 my-sm-0 btn-success" type="submit"><i class="fas fa-search"></i>
                        </button>
                    </form>
                </div>
            </div>


            <!--////////////////////////////////Pagination End //////////////////////////////////////////////-->


            <div class="row mt-3 text-center ">


				<?php

				foreach ( $someRecords as $author ) {
					$authorId    = $author["author_id"];
					$authorName  = $author["author_name"];
					$authorImage = $author["author_image"];

					if ( $authorImage == null || $authorImage == "" ) {
						$authorImage = "default.png";
					}

					echo "
	            
	           
            <div class='col-md-2 pb-4'>
                 <a href='search.php?authorId=$authorId' class='catLink'>
                    <img class='img-fluid rounded-circle w-40' src='img/authors/$authorImage' alt='$authorName'>
                    <p class='mt-2'>$authorName</p>
                </a>  
            </div>
                          
	            
	            ";
				}

				?>


            </div>


        </div>

    </section>


<?php
include 'footer.php';
?>