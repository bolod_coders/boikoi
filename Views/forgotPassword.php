<?php

include "header.php";

?>

<section>


    <div class="container border mt-3 bg-white">

        <div class="row">

            <div class="col-md-6 mt-5 p-5">
                <img src="img/authCover.jpg" alt="" class="img-fluid">
            </div>

            <div class="col-md-5 mt-5">
                <form action="recoverPass.php" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Input your Email address to recover password</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                               placeholder="Enter email" name="recovermail">

                    </div>


                    <button type="submit" class="btn btn-primary auth_btn">Recover</button>


                </form>
            </div>
        </div>
    </div>
</section>


<?php

include "footer.php";

?>
