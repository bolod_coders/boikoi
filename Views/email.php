<?php

/*
 * 1. Gmail Less Secure: https://myaccount.google.com/lesssecureapps
 * 2. Disable Unlock Captcha: https://accounts.google.com/DisplayUnlockCaptcha
 * 3. Gmail Settings: Forwarding and POP/IMAP: Enable IMAP: https://mail.google.com/mail/ca/u/0/#settings/fwdandpop
 *
 * 4. SETUP YOUR EMAIL & PASSWORD BELOW
 * */


// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

//Load Composer's autoloader
require_once "../vendor/autoload.php";

require_once '../vendor/phpmailer/phpmailer/src/PHPMailer.php';

if ( isset( $_GET['email'] ) ) {
	$userEmail = $_GET['email'];
}
if ( isset( $_GET['hash'] ) ) {
	$userHash = $_GET['hash'];
}
if ( isset( $_GET['fullName'] ) ) {
	$userFullName = $_GET['fullName'];
}

$userToken = $userHash . $userEmail;


$mail = new PHPMailer( true );                              // Passing `true` enables exceptions
try {
	//Server settings
	$mail->SMTPDebug = 0;                                 // Enable verbose debug output
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	$mail->SMTPAuth   = true;                               // Enable SMTP authentication
	$mail->Username   = 'forkanalam295@gmail.com';                 // SMTP username
	$mail->Password   = '0199356793';                           // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port       = 587;                                    // TCP port to connect to

	//Recipients
	$mail->setFrom( 'admin@boikoi.com', 'BoiKoi.com' );
	$mail->addAddress( $userEmail, $userFullName );     // Add a recipient
	//$mail->addAddress('ellen@example.com');               // Name is optional
	//$mail->addReplyTo('info@example.com', 'Information');
	//$mail->addCC('cc@example.com');
	//$mail->addBCC('bcc@example.com');

	//Attachments
	//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

	//Content
	$mail->isHTML( true );                                  // Set email format to HTML
	$mail->Subject = 'Email Verification';
	$mail->Body    = 'Please click this link to verify the email: http://localhost/teamProjects/BoiKoi/Views/verifyEmail.php?token=' .
	                 $userToken . "&email=$userEmail";
	//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	$mail->send();
	echo 'Message has been sent';
	\App\Utility::redirect( 'authentication.php?tab=signup' );
} catch ( Exception $e ) {
	\App\Message::message( 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo );
	\App\Utility::redirect( 'authentication.php?tab=signup' );


}