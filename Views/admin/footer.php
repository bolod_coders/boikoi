<footer class="main-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <p>BoiAche &copy; 2018-2019</p>
            </div>
            <div class="col-sm-6 text-right">
                <p>Developed by <a href="" class="external">ProGrammer</a></p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions and it helps me to run Bootstrapious. Thank you for understanding :)-->
            </div>
        </div>
    </div>
</footer>
</div>
<!-- JavaScript files-->
<script src="Resource/vendor/jquery/jquery.min.js"></script>
<script src="Resource/vendor/popper.js/umd/popper.min.js"></script>
<script src="Resource/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="Resource/js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
<script src="Resource/vendor/jquery.cookie/jquery.cookie.js"></script>
<script src="Resource/vendor/chart.js/Chart.min.js"></script>
<script src="Resource/vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="Resource/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="Resource/js/charts-home.js"></script>
<!-- Main File-->
<script src="Resource/js/front.js"></script>


<!--

OLD DASHBOARD

-->


<!-- Core plugin JavaScript-->
<script src="Resource/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="Resource/vendor/datatables/jquery.dataTables.js"></script>
<script src="Resource/vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Demo scripts for this page-->
<script src="Resource/js/demo/datatables-demo.js"></script>


<!-- Searchable Options JQuery Plugin (working but not used yet) -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="Resource/Bootstrap-4-Multi-Select-BsMultiSelect/dist/js/BsMultiSelect.js"></script>
<script>$("#bookAuthorSelect").bsMultiSelect();</script>-->

<!-- Include Editor JS files. -->
<script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/js/froala_editor.pkgd.min.js"></script>

<!-- Initialize the editor. -->
<script> $(function () {
        $('textarea').froalaEditor()
    }); </script>


<!--Selected item delete and publish-->
<script>


    var selectedButtons;

    function enabledOrDisabledSelectedButtons() {


        window.selectedButtons = false;

        $('.checkbox').each(function () { //iterate all listed checkbox items
            if (this.checked) {
                window.selectedButtons = true;
            }
        });

        if (!window.selectedButtons) {
            $("#publishSelected").attr("disabled", "disabled");
            $("#unPublishSelected").attr("disabled", "disabled");
            $("#deleteSelected").attr("disabled", "disabled");
        }
        else {
            $("#publishSelected").removeAttr('disabled');
            $("#unPublishSelected").removeAttr('disabled');
            $("#deleteSelected").removeAttr('disabled');
        }


    }


    $(document).ready(function () {

///////////////////////////////////////////////////////////////////////////
        $('#publishSelected').click(function () {
            document.forms[0].action = 'publishSelected.php';
            document.forms[0].submit();


        });

        $('#unPublishSelected').click(function () {
            document.forms[0].action = 'unPublishedSelected.php';
            document.forms[0].submit();


        });


        $('#deleteSelected').click(function () {

            var result = confirm("Are you sure, you want to delete the selected records?");
            if (result) {
                document.forms[0].action = "deleteSelected.php";
                document.forms[0].submit();
            }

        });


        enabledOrDisabledSelectedButtons();

        ////////////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////////////////////

        $('#SelectAll').on('click', function () {
            if (this.checked) {
                $('.checkbox').each(function () {
                    this.checked = true;

                });
            } else {
                $('.checkbox').each(function () {
                    this.checked = false;

                });
            }

            enabledOrDisabledSelectedButtons();

        });

        $('.checkbox').on('click', function () {
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $('#SelectAll').prop('checked', true);

            } else {
                $('#SelectAll').prop('checked', false);

            }

            enabledOrDisabledSelectedButtons();
        });


    });

    ////////////////////////////////////////////////////////////////////

</script>

<script>


    $('#message').fadeOut(500);
    $('#message').fadeIn(500);
    $('#message').fadeOut(500);
    $('#message').fadeIn(500);
    $('#message').fadeOut(1500);


    function confirmdel() {

        var delconfirmation = confirm("are you sure");
        return delconfirmation;

    }


</script>



</body>
</html>