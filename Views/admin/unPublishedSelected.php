<?php
/**
 * Created by PhpStorm.
 * User: Shuvo
 * Date: 1/16/2019
 * Time: 3:15 AM
 */


require_once '../../vendor/autoload.php';


$publishSelected = new \App\Book();


$allSuccess = true;

foreach ( $_POST["selected"] as $value ) {

	$publishSelected->setData( [ "bookId" => $value ] );


	if ( ! $publishSelected->publish_book_selected( 0 ) ) {

		$allSuccess = false;
	}


}

if ( $allSuccess ) {

	\App\Message::message( "Unpublished successfully!<br>" );
} else {
	\App\Message::message( "Unpublished unsuccessful!<br>" );
}


\App\Utility::redirect( "books.php" );



