<?php
/**
 * Created by PhpStorm.
 * User: Forkan
 * Date: 1/17/2019
 * Time: 11:06 PM
 */

include_once "header.php";

/* Dashboard Book List */
$books = new \App\Book();
$books = $books->admin_bookList();
$books = json_decode( json_encode( $books ), true );
?>


<!-- Breadcrumb-->
<div class="breadcrumb-holder">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Books</li>
        </ul>
    </div>
</div>

<section class="form mt-4 mb-4">

    <div class="container-fluid">

        <!-- DataTables Example -->
        <form name='SelectedForm' id='SelectedForm' method='post'>
            <div class="card">
                <div class="card-header">
                    <span class="h3">Book List</span>
                    <div class="float-right">
                        <button id="publishSelected" class="btn btn-warning">Publish</button>
                        <button id="unPublishSelected" class="btn btn-warning">Unpublish</button>
                        <button id="deleteSelected" class="btn btn-danger">Delete</button>
                        <a href="addBook.php" class="btn btn-success">Add New Book</a>
                    </div>


                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" cellspacing="0">
                            <thead>
                            <tr>
                                <th><input type="checkbox" id="SelectAll"></th>
                                <th>ID</th>
                                <th>Cover</th>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Category</th>
                                <th>Publisher</th>
                                <th>Price</th>
                                <th>Stock</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>


							<?php


							foreach ( $books as $book ) {
								$bookId    = $book["book_id"];
								$bookCover = $book["book_cover"];
								if ( $bookCover == "" || $bookCover == null ) {
									$bookCover = "default.png";
								}
								$bookName        = $book["book_name"];
								$bookAuthor      = $book["author_name"];
								$bookCategory    = $book["category_name"];
								$bookPublisher   = $book["publisher_name"];
								$bookPrice       = $book["book_price"];
								$bookStock       = $book["book_in_stock"];
								$bookIsPublished = $book["book_is_published"];

								/* Author */
								$authorID = $book["author_id"];

								/* Category */
								$categoryID = $book["category_id"];

								/* Publisher */
								$publisherID = $book["publisher_id"];

								echo "
	                                <tr>
	                                    <td> <input type='checkbox' class='checkbox'  name='selectedBook[]'                                                                                  value='$bookId'> </td> 
	                                    <td>$bookId</td>
                                        <td class='text-center'>
                                            <a href='../bookDetails.php?book_id=$bookId'>
                                                <img src='../img/books/$bookCover' width='80px'/>
                                            </a>
                                        </td>
                                        <td> 
                                            <a href='../bookDetails.php?book_id=$bookId' class='catLink'>$bookName</a>
                                        </td>
                                        <td>
                                            <a href='../search.php?authorID=$authorID' class='catLink'>$bookAuthor</a>
                                        </td>
                                        <td>
                                            <a href='../search.php?categoryID=$categoryID' class='catLink'>$bookCategory</a>
                                        </td>
                                        <td>
                                            <a href='../search.php?publisherID=$publisherID' class='catLink'>$bookPublisher</a>
                                        </td>
                                        <td>$bookPrice</td>
                                        <td>$bookStock</td>
                                        <td>";


								if ( $bookIsPublished == 0 ) {
									echo "<a href='publishBook.php?book_id=$bookId&publish_book=1' class='btn btn-info '>Publish</a>";
								} else {
									echo "<a href='publishBook.php?book_id=$bookId&publish_book=0' class='btn btn-outline-info '>Unpublish</a>";
								}


								echo "  
                                            
                                            <a href='editBook.php?book_id=$bookId' class='btn btn-success '><i class='fas fa-edit'></i></a>
                                            
                                            <a href='deleteBook.php?book_id=$bookId' class='btn btn-danger'  onclick='return confirmdel()'><i class='fas fa-trash'></i></a>
                                        </td>
                                    </tr>     

	            
                            	            ";
							}

							?>


                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>
        </form>
    </div>
    <!-- /.container-fluid -->
</section>


<?php
include_once "footer.php"
?>

