<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/10/2019
 * Time: 9:01 PM
 */

require_once "../../vendor/autoload.php";
$books = new \App\Book();

$books->setId( $_GET["book_id"] );

$books->publish_book( $_GET["publish_book"] );

\App\Utility::redirect( "books.php" );