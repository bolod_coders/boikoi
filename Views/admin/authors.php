<?php
/**
 * Created by PhpStorm.
 * User: Forkan
 * Date: 1/17/2019
 * Time: 11:06 PM
 */

include_once "header.php";

/* Dashboard Book List */
$authors = new \App\Author(null, null, null, null, null);
$authors = $authors->showAuthorList();
$authors = json_decode(json_encode($authors), true);
?>


<!-- Breadcrumb-->
<div class="breadcrumb-holder">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Books</li>
        </ul>
    </div>
</div>

<section class="form mt-4 mb-4">

    <div class="container-fluid">

        <!-- DataTables Example -->
        <form name='SelectedForm' id='SelectedForm' method='post'>
            <div class="card">
                <div class="card-header">
                    <span class="h3">Author List</span>
                    <div class="float-right">

                        <button id="deleteSelected" class="btn btn-danger">Delete</button>
                        <a href="addAuthor.php" class="btn btn-success">Add New Author</a>
                    </div>


                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" cellspacing="0">
                            <thead>
                            <tr>
                                <th><input type="checkbox" id="SelectAll"></th>
                                <th>ID</th>
                                <th>Author Cover</th>
                                <th>Author Name</th>
                                <th>Author Name En</th>
                                <th>Author Details</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>


                            <?php


                            foreach ($authors as $author) {
                                $authorId = $author["author_id"];
                                $authorCover = $author["author_image"];
                                if ($authorCover == "" || $authorCover == null) {
                                    $authorCover = "default.png";
                                }
                                $authorName = $author["author_name"];
                                $authorNameEn = $author["author_name_en"];
                                $authorDetails = $author["author_details"];


                                echo "
	                                <tr>
	                                    <td> <input type='checkbox' class='checkbox'  name='selectedAuthor[]' value='$authorId'> </td> 
	                                    <td>$authorId</td>
                                        <td class='text-center'>
                                            <a href='../search.php?author_id=$authorId'>
                                                <img src='../img/authors/$authorCover' width='80px'/>
                                            </a>
                                        </td>
                                        <td> 
                                            <a href='../search.php?author_id=$authorId' class='catLink'>$authorName</a>
                                        </td>
                                         <td> 
                                            <a href='../search.php?author_id=$authorId' class='catLink'>$authorNameEn</a>
                                        </td>
                                        <td>
                                            <a href='../search.php?author_id=$authorId' class='catLink'>$authorDetails</a>
                                        </td>
                                       
                                        <td>";


                                echo "  
                                            
                                            <a href='editAuthor.php?author_id=$authorId' class='btn btn-success '><i class='fas fa-edit'></i></a>
                                            
                                            <a href='deleteAuthor.php?author_id=$authorId' class='btn btn-danger'  onclick='return confirmdel()'><i class='fas fa-trash'></i></a>
                                        </td>
                                    </tr>     

	            
                            	            ";
                            }

                            ?>


                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>
        </form>
    </div>
    <!-- /.container-fluid -->
</section>


<?php
include_once "footer.php"
?>

