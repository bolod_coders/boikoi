<?php
/**
 * Created by PhpStorm.
 * User: Forkan
 * Date: 1/8/2019
 * Time: 8:43 PM
 */


require_once "../../vendor/autoload.php";
$author = new \App\Author(null,null,null,null,null);




if ( $_FILES["authorCover"]["name"] != "" ) {
    // book cover start
    $fileName           = $_FILES["authorCover"]["name"];
    $fileNameArr        = explode( ".", $fileName );
    $fileName           = $fileNameArr[0] . time() . "." . $fileNameArr[1];
    $_POST["authorCover"] = $fileName;
    $source             = $_FILES["authorCover"]["tmp_name"];
    $destination        = "../img/authors/" . $fileName;
    move_uploaded_file( $source, $destination );
    // book cover end
} else {
    $_POST["authorCover"] = $_POST["authorCoverOld"];
}


$author->setId( $_POST["authorId"] );

$author->setData( $_POST );


$author->update_author();


\App\Utility::redirect( "authors.php" );
