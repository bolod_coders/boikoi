// Call the dataTables jQuery plugin
$(document).ready(function () {

    var table = $('#dataTable').DataTable();

// Sort by column 1 and then re-draw
    table
        .order([0, 'desc'])
        .draw();

});

