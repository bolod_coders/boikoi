<?php
/**
 * Created by PhpStorm.
 * User: Shuvo
 * Date: 1/26/2019
 * Time: 2:48 AM
 */

require_once "../../vendor/autoload.php";

$category_id = $_GET["category_id"];


$books = new \App\Category(null, null);
$books = $books->delete_category($category_id);
\App\Utility::redirect("categories.php");