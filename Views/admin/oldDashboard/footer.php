<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="../logout.php">Logout</a>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript-->
<script src="Resource/vendor/jquery/jquery.js"></script>
<script src="Resource/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="Resource/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="Resource/vendor/chart.js/Chart.min.js"></script>
<script src="Resource/vendor/datatables/jquery.dataTables.js"></script>
<script src="Resource/vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="Resource/js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="Resource/js/demo/datatables-demo.js"></script>
<script src="Resource/js/demo/chart-area-demo.js"></script>


<!-- Searchable Options JQuery Plugin (working but not used yet) -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="Resource/Bootstrap-4-Multi-Select-BsMultiSelect/dist/js/BsMultiSelect.js"></script>
<script>$("#bookAuthorSelect").bsMultiSelect();</script>-->

<!-- Include Editor JS files. -->
<script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/js/froala_editor.pkgd.min.js"></script>

<!-- Initialize the editor. -->
<script> $(function () {
        $('textarea').froalaEditor()
    }); </script>


<!--Selected item delete and publish-->
<script>


    var selectedButtons;

    function enabledOrDisabledSelectedButtons() {


        window.selectedButtons = false;

        $('.checkbox').each(function () { //iterate all listed checkbox items
            if (this.checked) {
                window.selectedButtons = true;
            }
        });

        if (!window.selectedButtons) {
            $("#publishSelected").attr("disabled", "disabled");
            $("#unPublishSelected").attr("disabled", "disabled");
            $("#deleteSelected").attr("disabled", "disabled");
        }
        else {
            $("#publishSelected").removeAttr('disabled');
            $("#unPublishSelected").removeAttr('disabled');
            $("#deleteSelected").removeAttr('disabled');
        }


    }


    $(document).ready(function () {

///////////////////////////////////////////////////////////////////////////
        $('#publishSelected').click(function () {
            document.forms[1].action = 'publishSelected.php';
            document.forms[1].submit();


        });

        $('#unPublishSelected').click(function () {
            document.forms[1].action = 'unPublishedSelected.php';
            document.forms[1].submit();


        });


        $('#deleteSelected').click(function () {

            var result = confirm("Are you sure, you want to delete the selected records?");
            if (result) {
                document.forms[1].action = "deleteSelected.php";
                document.forms[1].submit();
            }

        });


        enabledOrDisabledSelectedButtons();

        ////////////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////////////////////

        $('#SelectAll').on('click', function () {
            if (this.checked) {
                $('.checkbox').each(function () {
                    this.checked = true;

                });
            } else {
                $('.checkbox').each(function () {
                    this.checked = false;

                });
            }

            enabledOrDisabledSelectedButtons();

        });

        $('.checkbox').on('click', function () {
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $('#SelectAll').prop('checked', true);

            } else {
                $('#SelectAll').prop('checked', false);

            }

            enabledOrDisabledSelectedButtons();
        });


    });

    ////////////////////////////////////////////////////////////////////

</script>


</body>

</html>
