<?php
/* * Created by PhpStorm.
 * User: Forkan
 * Date: 1/6/2019
 * Time: 9:29 PM*/

include 'header.php';

require_once "../../vendor/autoload.php";

$book_id = $_GET["book_id"];


$books = new \App\Book();

$books->setId( $book_id );


$singleBook = $books->view_book();
$singleBook = json_decode( json_encode( $singleBook ), true );


$bookId        = $singleBook["book_id"];
$bookName      = $singleBook["book_name"];
$bookNameEn    = $singleBook["book_name_en"];
$bookCategory  = $singleBook["book_category"];
$bookAuthor    = $singleBook["book_author"];
$bookPublisher = $singleBook["book_publisher"];
$bookPrice     = $singleBook["book_price"];
$bookISBN      = $singleBook["book_isbn"];
$bookEdition   = $singleBook["book_edition"];
$bookPage      = $singleBook["book_page"];
$bookStock     = $singleBook["book_in_stock"];
$bookLanguage  = $singleBook["book_language"];
$bookIsOff     = $singleBook["book_is_off"];
$bookCover     = $singleBook["book_cover"];

$authors = new \App\Author( null, null, null, null );
$authors = $authors->showAuthorList();
$authors = json_decode( json_encode( $authors ), true );


$categories = new \App\Category( null, null );
$categories = $categories->showCategoryList();
$categories = json_decode( json_encode( $categories ), true );


$publishers = new \App\Publisher( null, null, null );
$publishers = $publishers->showPublisherList();
$publishers = json_decode( json_encode( $publishers ), true );

?>

    <div class="container" style="margin-top: 100px">

        <h3>Edit Book </h3>

        <form action="updateBook.php" method="post" enctype="multipart/form-data">
            <input name="bookId" type="hidden" class="form-control" value="<?= $bookId ?>">
            <input name="bookCoverOld" type="hidden" class="form-control" value="<?= $bookCover ?>">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Book Title (Bengali)</label>
                    <input name="bookName" type="text" class="form-control" required value="<?= $bookName ?>">
                </div>
                <div class="form-group col-md-6">
                    <label>Book Title (English)</label>
                    <input name="bookNameEn" type="text" class="form-control" value="<?= $bookNameEn ?>">
                </div>

                <div class="form-group">
                    <h4>Book Cover</h4>
                    <img src="../img/books/<?= $bookCover ?>" alt="" class="img-fluid w-25 mb-2">
                    <input type="file" class="form-control-file" name="bookCover">
                </div>

            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>Author</label>
                    <select name="bookAuthor" class="form-control">
						<?php

						foreach ( $authors as $author ) {
							$authorId       = $author["author_id"];
							$authorName     = $author["author_name"];
							$selectedAuthor = "";

							if ( $authorId == $bookAuthor ) {
								$selectedAuthor = "selected";
							}

							echo " 
                                      <option value='$authorId' $selectedAuthor>$authorName</option>
                                
                                ";

						}

						?>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label>Category</label>
                    <select name="bookCategory" class="form-control">
						<?php

						foreach ( $categories as $category ) {
							$categoryId       = $category["category_id"];
							$categoryName     = $category["category_name"];
							$selectedCategory = "";

							if ( $categoryId == $bookCategory ) {
								$selectedCategory = "selected";
							}

							echo " 
                                      <option value='$categoryId' $selectedCategory>$categoryName</option>
                                
                                ";

						}

						?>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label>Publisher</label>
                    <select name="bookPublisher" class="form-control">


						<?php

						foreach ( $publishers as $publisher ) {
							$publisherId       = $publisher["publisher_id"];
							$publisherName     = $publisher["publisher_name"];
							$selectedPublisher = "";

							if ( $publisherId == $bookPublisher ) {
								$selectedPublisher = "selected";
							}

							echo " 
                                      <option value='$publisherId' $selectedPublisher>$publisherName</option>
                                
                                ";

						}

						?>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-3">
                    <label>Price</label>
                    <input name="bookPrice" type="number" class="form-control" value="<?= $bookPrice ?>">
                </div>
                <div class="form-group col-md-3">
                    <label>ISBN</label>
                    <input name="bookISBN" type="number" class="form-control" value="<?= $bookISBN ?>">
                </div>
                <div class="form-group col-md-3">
                    <label>Edition</label>
                    <input name="bookEdition" type="text" class="form-control" value="<?= $bookEdition ?>">
                </div>
                <div class="form-group col-md-3">
                    <label>Page</label>
                    <input name="bookPage" type="number" class="form-control" value="<?= $bookPage ?>">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>Stock</label>
                    <input name="bookInStock" type="number" class="form-control" value="<?= $bookStock ?>">
                </div>
                <div class="form-group col-md-4">
                    <label>Language</label>
                    <select name="bookLanguage" class="form-control">
                        <option value="বাংলা" <?php if ( $bookLanguage == 'বাংলা' )
							echo 'selected' ?>>বাংলা
                        </option>
                        <option value="English" <?php if ( $bookLanguage == 'English' )
							echo 'selected' ?>>English
                        </option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label>Off</label>
                    <input name="bookOff" type="number" class="form-control" value="<?= $bookIsOff ?>">
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-block p-4">Edit Book</button>
        </form>

    </div>


<?php

include 'footer.php'

?>