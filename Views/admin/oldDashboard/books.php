<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/29/2018
 * Time: 8:38 PM
 */

include_once 'header.php';

/* Dashboard Book List */
$books = new \App\Book();
$books = $books->admin_bookList();
$books = json_decode( json_encode( $books ), true );
?>


<div id="wrapper">

    <!-- Sidebar -->


    <ul class="sidebar navbar-nav fixed-top">
        <li class="nav-item">
            <a class="nav-link" href="index.php">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span>
            </a>
        </li>

        <li class="nav-item active">
            <a class="nav-link " href="books.php">
                <i class="fas fa-book-open"></i>
                <span>Books</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="categoryPage/authors.php">
                <i class="fas fa-pen-alt"></i>
                <span>Authors</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="categoryPage/categories.php">
                <i class="fas fa-sitemap"></i>
                <span>Categories</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="categoryPage/publishers.php">
                <i class="fas fa-book"></i>
                <span>Publishers</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="categoryPage/orders.php">
                <i class="fas fa-shopping-cart"></i>
                <span>Orders</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="categoryPage/users.php">
                <i class="fas fa-user-friends"></i>
                <span>Users</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="categoryPage/reviews.php">
                <i class="fas fa-comment"></i>
                <span>Reviews</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="categoryPage/reports.php">
                <i class="fas fa-bug"></i>
                <span>Reports</span></a>
        </li>

    </ul>


    <div class="offset-2 col-md-10">
        <div id="content-wrapper">

            <div class="container-fluid">

                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Books</li>
                </ol>


                <!-- DataTables Example -->
                <form name='SelectedForm' id='SelectedForm' method='post'>
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fas fa-table fa-1x"></i>
                            <span class="h5">Book List</span>
                            <div class="float-right ml-2">
                                <button id="publishSelected" class="btn btn-warning">Publish Selected</button>
                                <button id="unPublishSelected" class="btn btn-warning">Unpublished Selected</button>
                                <button id="deleteSelected" class="btn btn-danger ml-1">Delete Selected</button>

                            </div>

                            <a href="addBook.php" class="btn btn-success float-right">Add New Book</a>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" id="SelectAll">Select All</th>
                                        <th>ID</th>
                                        <th>Cover</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Category</th>
                                        <th>Publisher</th>
                                        <th>Price</th>
                                        <th>Stock</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>


									<?php


									foreach ( $books as $book ) {
										$bookId    = $book["book_id"];
										$bookCover = $book["book_cover"];
										if ( $bookCover == "" || $bookCover == null ) {
											$bookCover = "default.png";
										}
										$bookName        = $book["book_name"];
										$bookAuthor      = $book["author_name"];
										$bookCategory    = $book["category_name"];
										$bookPublisher   = $book["publisher_name"];
										$bookPrice       = $book["book_price"];
										$bookStock       = $book["book_in_stock"];
										$bookIsPublished = $book["book_is_published"];

										/* Author */
										$authorID = $book["author_id"];

										/* Category */
										$categoryID = $book["category_id"];

										/* Publisher */
										$publisherID = $book["publisher_id"];

										echo "
	                                <tr>
	                                    <td> <input type='checkbox' class='checkbox'  name='selected[]'                                                                                  value='$bookId'> </td> 
	                                    <td width='20px'>$bookId</td>
                                        <td width='50px' class='text-center'>
                                            <a href='../bookDetails.php?book_id=$bookId'>
                                                <img src='../img/books/$bookCover' width='80px'/>
                                            </a>
                                        </td>
                                        <td width='100px'> 
                                            <a href='../bookDetails.php?book_id=$bookId' class='catLink'>$bookName</a>
                                        </td>
                                        <td width='100px'>
                                            <a href='../search.php?authorID=$authorID' class='catLink'>$bookAuthor</a>
                                        </td>
                                        <td width='100px'>
                                            <a href='../search.php?categoryID=$categoryID' class='catLink'>$bookCategory</a>
                                        </td>
                                        <td width='100px'>
                                            <a href='../search.php?publisherID=$publisherID' class='catLink'>$bookPublisher</a>
                                        </td>
                                        <td width='50px'>$bookPrice</td>
                                        <td width='50px'>$bookStock</td>
                                        <td>";


										if ( $bookIsPublished == 0 ) {
											echo "<a href='publishBook.php?book_id=$bookId&publish_book=1' class='btn btn-info '>Publish</a>";
										} else {
											echo "<a href='publishBook.php?book_id=$bookId&publish_book=0' class='btn btn-outline-info '>Unpublish</a>";
										}


										echo "  
                                            
                                            <a href='editBook.php?book_id=$bookId' class='btn btn-success '><i class='fas fa-edit'></i></a>
                                            
                                            <a href='deleteBook.php?book_id=$bookId' class='btn btn-danger'  onclick='return confirmdel()'><i class='fas fa-trash'></i></a>
                                        </td>
                                    </tr>     

	            
                            	            ";
									}

									?>


                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                    </div>
                </form>
            </div>
            <!-- /.container-fluid -->

            <!-- Sticky Footer -->
            <footer class="sticky-footer">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright © Your Website 2018</span>
                    </div>
                </div>
            </footer>

        </div>
    </div>
    <!-- /.content-wrapper -->

</div>


<?php
include_once 'footer.php';
?>

<script>

    function confirmdel() {

        var delconfirmation = confirm("are you sure");
        return delconfirmation;

    }

</script>
