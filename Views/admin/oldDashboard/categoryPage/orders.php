<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/29/2018
 * Time: 8:38 PM
 */


require_once "../../vendor/autoload.php";

if ( ! isset( $_SESSION ) ) {
	session_start();
}

//if User click on a Remember me option then the if block is execute(Cookies) ,otherwise the else block is execute(Session)..

$userEmail = "";
if ( isset( $_COOKIE['user_email'] ) ) {
	$jsonDecodedObj = json_decode( $_COOKIE['user_email'] );
	$userEmail      = $jsonDecodedObj->UserEmail;
} elseif ( isset( $_SESSION['email'] ) ) {
	$userEmail = $_SESSION['email'];
}


$person = new \App\Person( null, null, null );
$person = $person->checkAdmin( $userEmail );


if ( $person == false ) {

	\App\Utility::redirect( "../404.php" );
}

include_once 'header.php';

?>


    <div id="wrapper">

        <!-- Sidebar -->


        <ul class="sidebar navbar-nav fixed-top">
            <li class="nav-item">
                <a class="nav-link" href="../index.php">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link " href="../books.php">
                    <i class="fas fa-book-open"></i>
                    <span>Books</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="authors.php">
                    <i class="fas fa-pen-alt"></i>
                    <span>Authors</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="categories.php">
                    <i class="fas fa-sitemap"></i>
                    <span>Categories</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="publishers.php">
                    <i class="fas fa-book"></i>
                    <span>Publishers</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="orders.php">
                    <i class="fas fa-shopping-cart"></i>
                    <span>Orders</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="users.php">
                    <i class="fas fa-user-friends"></i>
                    <span>Users</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="reviews.php">
                    <i class="fas fa-comment"></i>
                    <span>Reviews</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="reports.php">
                    <i class="fas fa-bug"></i>
                    <span>Reports</span></a>
            </li>

        </ul>


        <div class="offset-2 col-md-10">
            <div id="content-wrapper" class="down">

                <div class="container-fluid">

                    <!-- Breadcrumbs-->
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Orders</li>
                    </ol>


                    <!-- DataTables Example -->
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fas fa-table"></i>
                            Order List
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>Office</th>
                                        <th>Age</th>
                                        <th>Start date</th>
                                        <th>Salary</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>Office</th>
                                        <th>Age</th>
                                        <th>Start date</th>
                                        <th>Salary</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <tr>
                                        <td>Tiger Nixon</td>
                                        <td>System Architect</td>
                                        <td>Edinburgh</td>
                                        <td>61</td>
                                        <td>2011/04/25</td>
                                        <td>$320,800</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                    </div>

                </div>
                <!-- /.container-fluid -->

                <!-- Sticky Footer -->
                <footer class="sticky-footer">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright © Your Website 2018</span>
                        </div>
                    </div>
                </footer>

            </div>
        </div>
        <!-- /.content-wrapper -->

    </div>


<?php
include_once 'footer.php';
?>