<?php
/**
 * Created by PhpStorm.
 * User: Forkan
 * Date: 1/12/2019
 * Time: 11:28 PM
 */

require_once '../../vendor/autoload.php';


$publishSelected = new \App\Book();


$allSuccess = true;

foreach ( $_POST["selected"] as $value ) {

	$publishSelected->setData( [ "bookId" => $value ] );


	if ( ! $publishSelected->publish_book_selected( 1 ) ) {

		$allSuccess = false;
	}


}

if ( $allSuccess ) {

	\App\Message::message( "All data has been trashed successfully<br>" );
} else {
	\App\Message::message( "All data has not been trashed due to some error(s) <br>" );
}


\App\Utility::redirect( "books.php" );



