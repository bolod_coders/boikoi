<?php
/**
 * Created by PhpStorm.
 * User: Forkan
 * Date: 1/6/2019
 * Time: 8:55 PM
 */
require_once "../../vendor/autoload.php";

$book_id = $_GET["book_id"];


$books = new \App\Book();
$books = $books->delete_book( $book_id );
\App\Utility::redirect( "books.php" );

