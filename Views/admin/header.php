<?php

require_once "../../vendor/autoload.php";

if ( ! isset( $_SESSION ) ) {
	session_start();
}

//if User click on a Remember me option then the if block is execute(Cookies) ,otherwise the else block is execute(Session)..

$userEmail = "";
if ( isset( $_COOKIE['user_email'] ) ) {
	$jsonDecodedObj = json_decode( $_COOKIE['user_email'] );
	$userEmail      = $jsonDecodedObj->UserEmail;
} elseif ( isset( $_SESSION['email'] ) ) {
	$userEmail = $_SESSION['email'];
}


$person = new \App\Person( null, null, null );
$person = $person->checkAdmin( $userEmail );


if ( $person == false ) {

	\App\Utility::redirect( "../404.php" );
}

?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bootstrap Dashboard by Bootstrapious.com</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="Resource/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="Resource/vendor/fontawesome-free/css/all.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="Resource/css/fontastic.css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="Resource/css/grasp_mobile_progress_circle-1.0.0.min.css">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="Resource/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="Resource/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="Resource/css/custom.css">
    <!-- Favicon-->
    <!--<link rel="shortcut icon" href="img/favicon.ico">-->
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

    <!--
        OLD DASHBOARD
    -->
    <!-- Searchable Options -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>

    <!-- Include Editor style. -->
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_editor.pkgd.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_style.min.css" rel="stylesheet"
          type="text/css"/>
</head>
<body>
<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                <h2 class="h5">Admin</h2><span><?= $userEmail ?></span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center">
                    <strong>B</strong><strong class="text-primary">D</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">
                        <i class="fas  fa-tachometer-alt"></i>

                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="books.php">
                        <i class="fas fa-book-open"></i>
                        <span>Books</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="authors.php">
                        <i class="fas fa-pen-alt"></i>
                        <span>Authors</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="categories.php">
                        <i class="fas fa-sitemap"></i>
                        <span>Categories</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="publishers.php">
                        <i class="fas fa-book"></i>
                        <span>Publishers</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="categoryPage/orders.php">
                        <i class="fas fa-shopping-cart"></i>
                        <span>Orders</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="categoryPage/users.php">
                        <i class="fas fa-user-friends"></i>
                        <span>Users</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="categoryPage/reviews.php">
                        <i class="fas fa-comment"></i>
                        <span>Reviews</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="categoryPage/reports.php">
                        <i class="fas fa-bug"></i>
                        <span>Reports</span></a>
                </li>

            </ul>
        </div>

    </div>
</nav>

<div class="page">
    <!-- navbar-->
    <header class="header">
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                    <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i
                                    class="icon-bars"> </i></a><a href="index.html" class="navbar-brand">
                            <div class="brand-text d-none d-md-inline-block"><span>BoiAche </span><strong
                                        class="text-primary"> Dashboard</strong></div>
                        </a></div>
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                        <!-- Notifications dropdown-->
                        <!--                        <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-bell"></i><span class="badge badge-warning">12</span></a>-->
                        <!--                            <ul aria-labelledby="notifications" class="dropdown-menu">-->
                        <!--                                <li><a rel="nofollow" href="#" class="dropdown-item">-->
                        <!--                                        <div class="notification d-flex justify-content-between">-->
                        <!--                                            <div class="notification-content"><i class="fa fa-envelope"></i>You have 6 new messages </div>-->
                        <!--                                            <div class="notification-time"><small>4 minutes ago</small></div>-->
                        <!--                                        </div></a></li>-->
                        <!--                                <li><a rel="nofollow" href="#" class="dropdown-item">-->
                        <!--                                        <div class="notification d-flex justify-content-between">-->
                        <!--                                            <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>-->
                        <!--                                            <div class="notification-time"><small>4 minutes ago</small></div>-->
                        <!--                                        </div></a></li>-->
                        <!--                                <li><a rel="nofollow" href="#" class="dropdown-item">-->
                        <!--                                        <div class="notification d-flex justify-content-between">-->
                        <!--                                            <div class="notification-content"><i class="fa fa-upload"></i>Server Rebooted</div>-->
                        <!--                                            <div class="notification-time"><small>4 minutes ago</small></div>-->
                        <!--                                        </div></a></li>-->
                        <!--                                <li><a rel="nofollow" href="#" class="dropdown-item">-->
                        <!--                                        <div class="notification d-flex justify-content-between">-->
                        <!--                                            <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>-->
                        <!--                                            <div class="notification-time"><small>10 minutes ago</small></div>-->
                        <!--                                        </div></a></li>-->
                        <!--                                <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong> <i class="fa fa-bell"></i>view all notifications                                            </strong></a></li>-->
                        <!--                            </ul>-->
                        <!--                        </li>-->
                        <!-- Messages dropdown-->
                        <!--                        <li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-envelope"></i><span class="badge badge-info">10</span></a>-->
                        <!--                            <ul aria-labelledby="notifications" class="dropdown-menu">-->
                        <!--                                <li><a rel="nofollow" href="#" class="dropdown-item d-flex">-->
                        <!--                                        <div class="msg-profile"> <img src="Resource/img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle"></div>-->
                        <!--                                        <div class="msg-body">-->
                        <!--                                            <h3 class="h5">Jason Doe</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>-->
                        <!--                                        </div></a></li>-->
                        <!--                                <li><a rel="nofollow" href="#" class="dropdown-item d-flex">-->
                        <!--                                        <div class="msg-profile"> <img src="Resource/img/avatar-2.jpg" alt="..." class="img-fluid rounded-circle"></div>-->
                        <!--                                        <div class="msg-body">-->
                        <!--                                            <h3 class="h5">Frank Williams</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>-->
                        <!--                                        </div></a></li>-->
                        <!--                                <li><a rel="nofollow" href="#" class="dropdown-item d-flex">-->
                        <!--                                        <div class="msg-profile"> <img src="Resource/img/avatar-3.jpg" alt="..." class="img-fluid rounded-circle"></div>-->
                        <!--                                        <div class="msg-body">-->
                        <!--                                            <h3 class="h5">Ashley Wood</h3><span>sent you a direct message</span><small>3 days ago at 7:58 pm - 10.06.2014</small>-->
                        <!--                                        </div></a></li>-->
                        <!--                                <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong> <i class="fa fa-envelope"></i>Read all messages    </strong></a></li>-->
                        <!--                            </ul>-->
                        <!--                        </li>-->

                        <!-- Languages dropdown    -->
                        <!--                        <li class="nav-item dropdown"><a id="languages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link language dropdown-toggle"><img src="Resource/img/flags/16/GB.png" alt="English"><span class="d-none d-sm-inline-block">English</span></a>-->
                        <!--                            <ul aria-labelledby="languages" class="dropdown-menu">-->
                        <!--                                <li><a rel="nofollow" href="#" class="dropdown-item"> <img src="Resource/img/flags/16/DE.png" alt="English" class="mr-2"><span>German</span></a></li>-->
                        <!--                                <li><a rel="nofollow" href="#" class="dropdown-item"> <img src="Resource/img/flags/16/FR.png" alt="English" class="mr-2"><span>French                                                         </span></a></li>-->
                        <!--                            </ul>-->
                        <!--                        </li>-->
                        <!-- Back to Homepage                       -->
                        <li class="nav-item"><a href="../index.php" class="nav-link logout"> <span
                                        class="d-none d-sm-inline-block">Back to Homepage</span><i
                                        class="fa fa-sign-out"></i></a></li>
                        <!-- Log out-->
                        <li class="nav-item"><a href="../logout.php" class="nav-link logout"> <span
                                        class="d-none d-sm-inline-block">Logout</span><i class="fa fa-sign-out"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div  id ="message" class="text-center"><?=  \App\Message::message()  ?></div>
