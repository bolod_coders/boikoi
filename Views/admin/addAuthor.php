<?php

include 'header.php';



?>

    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                <li class="breadcrumb-item active">Add New Book</li>
            </ul>
        </div>
    </div>

    <section class="form mt-4 mb-4">
        <div class="container-fluid">

            <div class="card">

                <div class="card-header">


                    <span class="h3">Add New Book</span>


                </div>

                <div class="card-body">

                    <form action="storeAuthor.php" method="post" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Author Name(Bangla)</label>
                                <input name="authorName" type="text" class="form-control" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Author Name (English)</label>
                                <input name="authorNameEn" type="text" class="form-control">
                            </div>

                            <div class="form-group  col-md-4 mt-2">
                                <label>Author Cover</label>
                                <input type="file" class="form-control-file" name="authorCover">
                            </div>

                        </div>



                        <div class="form-row mt-4">
                            <div class="form-group col-md-12">
                                <label>Author Details</label>
                                <textarea class="form-control" id="editor" name="authorDetails"></textarea>
                            </div>

                        </div>





                        <button type="submit" class="btn btn-primary btn-block p-4">Add New Author</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php

include 'footer.php';

?>