<?php
/**
 * Created by PhpStorm.
 * User: Shuvo
 * Date: 1/27/2019
 * Time: 3:55 AM
 */

include 'header.php';



?>

    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                <li class="breadcrumb-item active">Add New Book</li>
            </ul>
        </div>
    </div>

    <section class="form mt-4 mb-4">
        <div class="container-fluid">

            <div class="card">

                <div class="card-header">


                    <span class="h3">Add New Publisher</span>


                </div>

                <div class="card-body">

                    <form action="storePublisher.php" method="post" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>publisher Name(Bangla)</label>
                                <input name="authorName" type="text" class="form-control" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label>publisher Name (English)</label>
                                <input name="authorNameEn" type="text" class="form-control">
                            </div>

                            <div class="form-group  col-md-4 mt-2">
                                <label>publisher Cover</label>
                                <input type="file" class="form-control-file" name="authorCover">
                            </div>

                        </div>






                        <button type="submit" class="btn btn-primary btn-block p-4">Add New publisher</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php

include 'footer.php';

?>