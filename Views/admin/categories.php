<?php
/**
 * Created by PhpStorm.
 * User: Forkan
 * Date: 1/17/2019
 * Time: 11:06 PM
 */

include_once "header.php";

/* Dashboard Book List */
$categories = new \App\Category(null, null,null);
$categories = $categories->showCategoryList();
$categories = json_decode(json_encode($categories), true);

?>


<!-- Breadcrumb-->
<div class="breadcrumb-holder">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Books</li>
        </ul>
    </div>
</div>

<section class="form mt-4 mb-4">

    <div class="container-fluid">

        <!-- DataTables Example -->
        <form name='SelectedForm' id='SelectedForm' method='post'>
            <div class="card">
                <div class="card-header">
                    <span class="h3">Category List</span>
                    <div class="float-right">

                        <button id="deleteSelected" class="btn btn-danger">Delete</button>
                        <a href="addCategory.php" class="btn btn-success">Add New Category</a>
                    </div>


                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" cellspacing="0">
                            <thead>
                            <tr>
                                <th><input type="checkbox" id="SelectAll"></th>
                                <th>ID</th>

                                <th>Category Name</th>
                                <th>Category Name En</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>


                            <?php


                            foreach ($categories as $category) {
                                $categoryId = $category["category_id"];

                                $categoryName = $category["category_name"];
                                $categoryNameEn = $category["category_name_en"];


                                echo "
	                                <tr>
	                                    <td> <input type='checkbox' class='checkbox'  name='selectedCategory[]' value='$categoryId'> </td> 
	                                    <td>$categoryId</td>
                                        
                                        <td> 
                                            <a href='../search.php?category_id=$categoryId' class='catLink'>$categoryName</a>
                                        </td> 
                                         
                                         <td> 
                                            <a href='../search.php?category_id=$categoryId' class='catLink'>$categoryNameEn</a>
                                        </td>
                                        
                                       
                                        <td>";


                                echo "  
                                            
                                            <a href='editCategory.php?category_id=$categoryId' class='btn btn-success'><i class='fas fa-edit'></i></a>
                                            
                                            <a href='deleteCategory.php?category_id=$categoryId' class='btn btn-danger'  onclick='return confirmdel()'><i class='fas fa-trash'></i></a>
                                        </td>
                                    </tr>     

	            
                            	            ";
                            }

                            ?>


                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>
        </form>
    </div>
    <!-- /.container-fluid -->
</section>


<?php
include_once "footer.php"
?>

