<?php
/**
 * Created by PhpStorm.
 * User: Forkan
 * Date: 1/8/2019
 * Time: 8:43 PM
 */


require_once "../../vendor/autoload.php";
$books = new \App\Book();


if ($_FILES["bookCover"]["name"] != "") {
    // book cover start
    $fileName = $_FILES["bookCover"]["name"];
    $fileNameArr = explode(".", $fileName);
    $fileName = $fileNameArr[0] . time() . "." . $fileNameArr[1];
    $_POST["bookCover"] = $fileName;
    $source = $_FILES["bookCover"]["tmp_name"];
    $destination = "../img/books/" . $fileName;
    move_uploaded_file($source, $destination);
    // book cover end
} else {
    $_POST["bookCover"] = $_POST["bookCoverOld"];
}


$books->setId($_POST["bookId"]);

$books->setData($_POST);


$books->update_book();


\App\Utility::redirect("books.php");
