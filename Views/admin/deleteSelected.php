<?php
/**
 * Created by PhpStorm.
 * User: Forkan
 * Date: 1/12/2019
 * Time: 11:28 PM
 */

require_once '../../vendor/autoload.php';


if (isset($_POST['selectedAuthor'])) {

    $publishSelected = new \App\Author(null, null, null, null, null);

    $allSuccess = true;

    foreach ($_POST["selectedAuthor"] as $value) {

        $publishSelected->setData(["authorId" => $value]);


        if (!$publishSelected->delete_author($value)) {

            $allSuccess = false;
        }
    }

    if ($allSuccess) {

        \App\Message::message("All data has been deleted successfully<br>");
    } else {
        \App\Message::message("All data has not been deleted due to some error(s) <br>");
    }


    \App\Utility::redirect("authors.php");
}


if (isset($_POST['selectedBook'])) {

    $publishSelected = new \App\Book();

    $allSuccess = true;

    foreach ($_POST["selectedBook"] as $value) {

        $publishSelected->setData(["bookId" => $value]);


        if (!$publishSelected->delete_book($value)) {

            $allSuccess = false;
        }
    }

    if ($allSuccess) {

        \App\Message::message("All data has been deleted successfully<br>");
    } else {
        \App\Message::message("All data has not been deleted due to some error(s) <br>");
    }


    \App\Utility::redirect("books.php");
}



if (isset($_POST['selectedCategory'])) {

    $publishSelected = new \App\Category(null,null);
    $allSuccess = true;

    foreach ($_POST["selectedCategory"] as $value) {

        $publishSelected->setData(["categoryId" => $value]);


        if (!$publishSelected->delete_category($value)) {

            $allSuccess = false;
        }
    }

    if ($allSuccess) {

        \App\Message::message("All data has been deleted successfully<br>");
    } else {
        \App\Message::message("All data has not been deleted due to some error(s) <br>");
    }


    \App\Utility::redirect("categories.php");
}




if (isset($_POST['selectedPublisher'])) {

    $publishSelected = new \App\Publisher(null,null,null,null);
    $allSuccess = true;

    foreach ($_POST["selectedPublisher"] as $value) {

        $publishSelected->setData(["publihserId" => $value]);


        if (!$publishSelected->delete_publisher($value)) {

            $allSuccess = false;
        }
    }

    if ($allSuccess) {

        \App\Message::message("All data has been deleted successfully<br>");
    } else {
        \App\Message::message("All data has not been deleted due to some error(s) <br>");
    }


    \App\Utility::redirect("publishers.php");
}