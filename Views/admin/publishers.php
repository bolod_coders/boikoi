<?php
/**
 * Created by PhpStorm.
 * User: shuvo
 * Date: 1/72/2019
 * Time: 9:06 PM
 */

include_once "header.php";

/* Dashboard Book List */
$publishers = new \App\Publisher(null,null,null,null);
$publishers = $publishers->showPublisherList();
$publishers = json_decode(json_encode($publishers), true);



?>


<!-- Breadcrumb-->
<div class="breadcrumb-holder">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Books</li>
        </ul>
    </div>
</div>

<section class="form mt-4 mb-4">

    <div class="container-fluid">

        <!-- DataTables Example -->
        <form name='SelectedForm' id='SelectedForm' method='post'>
            <div class="card">
                <div class="card-header">
                    <span class="h3">Publishers List</span>
                    <div class="float-right">

                        <button id="deleteSelected" class="btn btn-danger">Delete</button>
                        <a href="addPublisher.php" class="btn btn-success">Add New Publisher</a>
                    </div>


                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" cellspacing="0">
                            <thead>
                            <tr>
                                <th><input type="checkbox" id="SelectAll"></th>
                                <th>ID</th>

                                <th>Publisher Name</th>
                                <th>Publisher Name En</th>
                                <th>Publisher Logo </th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>


                            <?php


                            foreach ($publishers as $publisher) {
                                $publisherId = $publisher["publisher_id"];

                                $publisherName = $publisher["publisher_name"];
                                $publisherNameEn = $publisher["publisher_name_en"];
                                $publisherlogo = $publisher["publisher_logo"];


                                echo "
	                                <tr>
	                                    <td> <input type='checkbox' class='checkbox'  name='selectedPublisher[]' value='$publisherId'> </td> 
	                                    <td>$publisherId</td>
                                        
                                        <td> 
                                            <a href='../search.php?publisher_id=$publisherId' class='catLink'>$publisherlogo</a>
                                        </td> 
                                         
                                        
                                        <td> 
                                            <a href='../search.php?publisher_id=$publisherId' class='catLink'>$publisherName</a>
                                        </td> 
                                         
                                         <td> 
                                            <a href='../search.php?publisher_id=$publisherId' class='catLink'>$publisherNameEn</a>
                                        </td>
                                        
                                       
                                        <td>";


                                echo "  
                                            
                                            <a href='editPublisher.php?publisher_id=$publisherId' class='btn btn-success'><i class='fas fa-edit'></i></a>
                                            
                                            <a href='deletePublisher.php?publisher_id=$publisherId' class='btn btn-danger'  onclick='return confirmdel()'><i class='fas fa-trash'></i></a>
                                        </td>
                                    </tr>     

	            
                            	            ";
                            }

                            ?>


                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>
        </form>
    </div>
    <!-- /.container-fluid -->
</section>


<?php
include_once "footer.php"
?>

