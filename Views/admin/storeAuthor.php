<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/2/2019
 * Time: 11:02 PM
 */

require_once "../../vendor/autoload.php";

$authors = new \App\Author(null, null, null, null, null);


if ($_FILES["authorCover"]["name"] != "") {
    // book cover start
    $fileName = $_FILES["authorCover"]["name"];
    $fileNameArr = explode(".", $fileName);
    $fileName = $fileNameArr[0] . time() . "." . $fileNameArr[1];
    $_POST["authorCover"] = $fileName;
    $source = $_FILES["authorCover"]["tmp_name"];
    $destination = "../img/authors/" . $fileName;
    move_uploaded_file($source, $destination);
    // book cover end
}


$authors->setData($_POST);
$authors = $authors->admin_add_new_author();


\App\Utility::redirect('authors.php');