<?php

include 'header.php';



?>

    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                <li class="breadcrumb-item active">Add New Book</li>
            </ul>
        </div>
    </div>

    <section class="form mt-4 mb-4">
        <div class="container-fluid">

            <div class="card">

                <div class="card-header">


                    <span class="h3">Add New Category</span>


                </div>

                <div class="card-body">

                    <form action="storeCategory.php" method="post" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Category Name</label>
                                <input name="categoryName" type="text" class="form-control" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label>Category Name En</label>
                                <input name="categoryNameEn" type="text" class="form-control" required>
                            </div>


                        </div>







                        <button type="submit" class="btn btn-primary btn-block p-4">Add New Category</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php

include 'footer.php';

?>