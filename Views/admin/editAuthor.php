<?php
/* * Created by PhpStorm.
 * User: Forkan
 * Date: 1/6/2019
 * Time: 9:29 PM*/

include 'header.php';

require_once "../../vendor/autoload.php";

$authorId = $_GET["author_id"];


$authors = new \App\Author(null,null,null,null,null);

$authors->setId( $authorId );


$singleAuthor = $authors->view_author();
$singleAuthor = json_decode( json_encode( $singleAuthor ), true );




$authorId        = $singleAuthor["author_id"];
$authorName      = $singleAuthor["author_name"];
$authorNameEn    = $singleAuthor["author_name_en"];
$authorCover  = $singleAuthor["author_image"];
$authorDetails    = $singleAuthor["author_details"];

?>

    <div class="container" style="margin-top: 100px">

        <h3>Edit Book </h3>

        <form action="updateAuthor.php" method="post" enctype="multipart/form-data">
            <input name="authorId" type="hidden" class="form-control" value="<?= $authorId ?>">
            <input name="authorCoverOld" type="hidden" class="form-control" value="<?= $authorCover ?>">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Author Title (Bengali)</label>
                    <input name="authorName" type="text" class="form-control" required value="<?= $authorName ?>">
                </div>
                <div class="form-group col-md-6">
                    <label>Author Title (English)</label>
                    <input name="authorNameEn" type="text" class="form-control" value="<?= $authorNameEn ?>">
                </div>

                <div class="form-group">
                    <h4>Author Cover</h4>
                    <img src="../img/authors/<?= $authorCover ?>" alt="" class="img-fluid w-25 mb-2">
                    <input type="file" class="form-control-file" name="authorCover">
                </div>


                <div class="form-row mt-4">
                    <div class="form-group col-md-12">
                        <label>Author Details</label>
                        <textarea class="form-control" id="editor" name="authorDetails"><?= $authorDetails ?></textarea>
                    </div>

                </div>
            </div>



            <button type="submit" class="btn btn-primary btn-block p-4">Edit Author</button>
        </form>

    </div>


<?php

include 'footer.php'

?>