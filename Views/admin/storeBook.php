<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/2/2019
 * Time: 11:02 PM
 */

require_once "../../vendor/autoload.php";

$books = new \App\Book();


if ( $_FILES["bookCover"]["name"] != "" ) {
	// book cover start
	$fileName           = $_FILES["bookCover"]["name"];
	$fileNameArr        = explode( ".", $fileName );
	$fileName           = $fileNameArr[0] . time() . "." . $fileNameArr[1];
	$_POST["bookCover"] = $fileName;
	$source             = $_FILES["bookCover"]["tmp_name"];
	$destination        = "../img/books/" . $fileName;
	move_uploaded_file( $source, $destination );
	// book cover end
}


$books->setData( $_POST );
$books = $books->admin_add_new_Books();


\App\Utility::redirect( 'books.php' );