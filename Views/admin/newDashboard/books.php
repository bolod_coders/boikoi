<?php
/**
 * Created by PhpStorm.
 * User: Forkan
 * Date: 1/17/2019
 * Time: 11:06 PM
 */
include_once "header.php";
?>

<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center"><img src="img/avatar-7.jpg" alt="person"
                                                               class="img-fluid rounded-circle">
                <h2 class="h5">Nathan Andrews</h2><span>Web Developer</span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center">
                    <strong>B</strong><strong class="text-primary">D</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <h5 class="sidenav-heading">Main</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li class="nav-item">
                    <a class="nav-link" href="dashboard.php">
                        <i class="fas  fa-tachometer-alt"></i>

                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="books.php">
                        <i class="fas fa-book-open"></i>
                        <span>Books</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="authors.php">
                        <i class="fas fa-pen-alt"></i>
                        <span>Authorssss</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="categoryPage/categories.php">
                        <i class="fas fa-sitemap"></i>
                        <span>Categories</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="categoryPage/publishers.php">
                        <i class="fas fa-book"></i>
                        <span>Publishers</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="categoryPage/orders.php">
                        <i class="fas fa-shopping-cart"></i>
                        <span>Orders</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="categoryPage/users.php">
                        <i class="fas fa-user-friends"></i>
                        <span>Users</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="categoryPage/reviews.php">
                        <i class="fas fa-comment"></i>
                        <span>Reviews</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="categoryPage/reports.php">
                        <i class="fas fa-bug"></i>
                        <span>Reports</span></a>
                </li>

            </ul>
        </div>
        <div class="admin-menu">
            <h5 class="sidenav-heading">Second menu</h5>
            <ul id="side-admin-menu" class="side-menu list-unstyled">
                <li><a href="#"> <i class="icon-screen"> </i>Demo</a></li>
                <li><a href="#"> <i class="icon-flask"> </i>Demo
                        <div class="badge badge-info">Special</div>
                    </a></li>
                <li><a href=""> <i class="icon-flask"> </i>Demo</a></li>
                <li><a href=""> <i class="icon-picture"> </i>Demo</a></li>
            </ul>
        </div>
    </div>
</nav>

<?php
include_once "footer.php"
?>

